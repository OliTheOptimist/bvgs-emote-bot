All Emote Stats:
?AllEmoteStats ('users'/'channels'/'emotes') [dd/mm/yyyy] [dd/mm/yyyy] [limit]

Gives emote stats for either users, channels or emotes
To use AllEmoteStats the command must be used with either the phrase 'users', 'channels' or 'emotes'

Dates can be used to limit the search:
    1 Date - From that date until the current date
    2 Dates - From the oldest date mentioned to the newest date mentioned

Limit:
    Limit is used to show more rows from the query, the default is 5