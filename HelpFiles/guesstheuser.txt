Guess the User:
?GuessTheUser

This starts a game where a random message is chosen at random from the most active users on the server. You have to guess who said it.