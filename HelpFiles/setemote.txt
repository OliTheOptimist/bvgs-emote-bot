?SetEmote"Role Name" Emote
THIS COMMAND CAN ONLY BE USED AFTER USING EditRoleGroup OR CreateRoleGroup

This changes the emote of a current existing role to be updated, this will put the role at the bottom of the list so the react order relates to the role order. You can use ?ordermessage to then order the list alphabetically if you wish.