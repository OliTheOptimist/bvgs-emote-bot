import datetime
import discord
import runsql
import parsemessage
import discordBotFunctions

async def addRole(message):
    if(not await discordBotFunctions.checkUser(message)):
        return

    #create duplicate dates to see if they are the same later
    tempDate = datetime.datetime.now()
    getTimeDefined = parsemessage.getTimeDefined(message.content, tempDate)
    timeTill = getTimeDefined['date']

    roles = parsemessage.getRoles(message, getRole = True)
    users = parsemessage.getUsers(message, getUser = True)

    if(not roles):
        await message.channel.send("No roles found to tag")
        return
    elif(not users):
        await message.channel.send("No users found to tag")
        return
    
    ##for each user and each role add the roles for a certain time
    for user in users:
        for role in roles:
            if(role in user.roles):
                await message.channel.send("You cannot add " + role.name + " to " + user.display_name + " as they already have this role")
                return
            await user.add_roles(role)
            if(timeTill == tempDate):
                await message.channel.send(role.name + " added to " + user.display_name)
            else:
                await message.channel.send(role.name + " added to " + user.display_name + " for " + getTimeDefined['dateString'])
                sql = "INSERT INTO role_until (user_id, role_id, server_id, until, channel_id) VALUES (%s,%s,%s,%s,%s)"
                parameters = [user.id, role.id, message.guild.id, timeTill, message.channel.id]
                runsql.executeSQL(sql, parameters)         
            


##delete roles that are overdue
async def deleteRoles(client):
    rows = runsql.executeSQL("SELECT * FROM role_until WHERE until < NOW()")
    if(not rows):
        return

    for row in rows:
        server = client.get_guild(row['server_id'])
        if(not server):
            continue

        role = server.get_role(row['role_id'])
        user = server.get_member(row['user_id'])
        if(not role or not user):
            continue

        channel = server.get_channel(row['channel_id'])
        if(not channel):
            channel = server.system_channel
            continue
        
        if(role in user.roles):
            await user.remove_roles(role)
            await channel.send(role.name + " removed from " + user.display_name)

    runsql.executeSQL("DELETE FROM role_until WHERE until < NOW()")


    




