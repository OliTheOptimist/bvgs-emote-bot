import runsql
import discordBotFunctions

class addValidChannels:
	validChannels = []

	async def addChannel(self, message):
		if(not await discordBotFunctions.checkUser(message)):
		    return
		sql = "INSERT IGNORE INTO valid_channels (channel_id,server_id) VALUES (%s,%s)"
		runsql.executeSQL(sql, [message.channel.id,message.guild.id])
		await message.channel.send("This channel has been added to valid channels")
		if not message.channel.id in self.validChannels:
			self.validChannels.append(message.channel.id)

	async def removeChannel(self, message):
		if(not await discordBotFunctions.checkUser(message)):
		    return
		sql = "DELETE FROM valid_channels WHERE channel_id = %s"
		runsql.executeSQL(sql, [message.channel.id])
		await message.channel.send("This channel has been removed from the valid channels")
		self.validChannels.remove(message.channel.id)

	def loadValidChannels(self):
		sql = runsql.executeSQL("SELECT * FROM valid_channels")
		for channel in sql:
			self.validChannels.append(channel["channel_id"])

	def isValidChannel(self, channel_id):
		for channel in self.validChannels:
			if channel == channel_id:
				return True
		return False

	async def showValidChannels(self, message):
		sql = runsql.executeSQL("SELECT channel_id FROM valid_channels WHERE server_id = %s",[message.guild.id])
		channel_message = ""
		for channel in sql:
			channel_message += message.guild.get_channel(channel["channel_id"]).mention + " "
		await message.channel.send(channel_message)
