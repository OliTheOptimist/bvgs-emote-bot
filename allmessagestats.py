import parsemessage
import runsql
import printStats

async def messageAllStats(message):
    groupBy = ""
    sqlSelector = " ";

    ##find out which catergory is mentioned in the message
    groupBy = ["user_id","channel_id"]
    sqlSelector = ["user_id as User, ", "channel_id as Channel, "]
    lookingForUser = 'user' in message.content.lower()
    lookingForChannel = 'channel' in message.content.lower()

    if(not lookingForUser and lookingForChannel):
        del groupBy[0]
        del sqlSelector[0]
    elif(lookingForUser and not lookingForChannel):
        del groupBy[1]
        del sqlSelector[1]

    ##get information from the string sent
    whereReturn = await parsemessage.parseMessageForSQL(message, users = True, emotes = False, channels = True, roles = True)
    for x in range(0,len(groupBy)):
        ##combine an sql statement based on what users have been selected
        sqlStart = "SELECT " + sqlSelector[x] + " COUNT(user_id) as Total FROM message WHERE server_id = %s " + whereReturn["String"] + " GROUP BY " + groupBy[x] + " ORDER BY total DESC " + parsemessage.workOutLimit(message)
        parameters = [message.guild.id] + whereReturn['Parameters']
        await printStats.printTable(message, runsql.executeSQL(sqlStart, parameters))






