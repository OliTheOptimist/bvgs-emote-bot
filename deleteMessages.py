import discordBotFunctions
import asyncio

async def deleteMessages(message):
    if(not await  discordBotFunctions.checkUser(message)):
        return

    if(not message.content.isdigit() and int(message.content) > 0):
        await message.channel.send("Please define the amount of messages you want to delete")
        return

    msglimit = int(message.content)
    await message.channel.purge(limit=msglimit + 1)
    if msglimit > 1:
        await message.channel.send("{0} messages deleted!".format(msglimit), delete_after=3)
    else:
        await message.channel.send("{0} message deleted!".format(msglimit), delete_after=3)
        