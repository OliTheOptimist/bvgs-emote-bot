import asyncio

def checkUserHasRole(user, role):
    for userRole in user.roles:
        if(userRole.id == role.id):
            return True
    return False

##checks user has the manage roles permission
async def checkUser(message):
    if(message.author.permissions_in(message.channel).administrator or message.author.id == 126087047260667904):
        return True
    await message.channel.send("You do not have the permissions to use this command")
    return False

##send a message, wait 5 seconds then delete both messages
async def messageWaitDelete(message, string):
    m = await message.channel.send(string)
    await asyncio.sleep(5) # task runs every 60 seconds
    await message.channel.delete_message(m)
    await message.channel.delete_message(message)

#Takes gets the role list from the server and the role ids from the sql and returns all role objects that are in the list
def getRolesFromIds(message, list, sql = True):
    roleArray = []
    for role in message.guild.roles:
        for row in list:
            if(sql):
                if(str(role.id) == str(row['role_id'])):
                    roleArray.append(role)
            else:
                 if(str(role.id) == str(row)):
                    roleArray.append(role)
    roleArray = sorted(roleArray, key=lambda x: x.name.lower())
    return roleArray

def getUsersFromIds(message, list, sql = True, names = False):
    userArray = []
    for user in list:
        user_object = message.guild.get_member(user['user_id'])
        if(not user_object):
            continue
            
        if(names):
            userArray.append(user_object.display_name)
        else:
            userArray.append(user_object)
    if(names):  
        userArray = sorted(userArray, key=lambda x: x.lower())
    else:
        userArray = sorted(userArray, key=lambda x: x.display_name.lower())
    return userArray

def extractValue(sqlReturn, role):
    listArray = []
    for sql in sqlReturn:
        listArray.append(sql[role])
    return listArray

def getRoleByString(message, string):
    for role in message.guild.roles:
        if(role.name.lower().replace(" ","") == string.lower().replace(" ","")):
            return role
    return False