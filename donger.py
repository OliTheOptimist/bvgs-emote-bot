import random
import emoji
import parsemessage

class Donger:
    dongerList = [];

    async def runDonger(self, message):
        #if the array list is full then don't import all the donger emotes
        #this saves time in the future
        if len(self.dongerList) == 0:
            with open("DongerList.txt", "r", encoding='utf-8') as dongerText:
                self.dongerList = dongerText.readlines()

        #if no message is given then just send a random command
        if(not message.content):
            await message.channel.send(self.dongerList[random.randint(0, len(self.dongerList) - 1)])
        else:
            #my attempt at a hash function
            dongerString = message.content.replace("?donger", "")
            alphabetList = ['m', 'b', 'a', 'z', 't', 'd', 'e', 'f', 'g', 'u', 'h', 'i', 'v', 'k', 'l',
            'n', 'c', 'o', 'p', 'q', 'r', 's', 'j', 'w', 'x', 'y', '1', '2', '3', '4', '9',
            '8', '7', '6', '5', ',', '.', ' ', '!']
            total = 0

            i = 21
            for emote in emoji.UNICODE_EMOJI:
                if(emote in dongerString):
                    total += i
                i += 1

            for c in dongerString:

                if(c in alphabetList):
                    total += alphabetList.index(c) * 3 + 1
                else:
                    total += 322
            await message.channel.send(self.dongerList[total % len(self.dongerList)])

