import runsql
import parsemessage
import printStats
import unicodedata

##gets all information from the emote_chain sql table
async def runCommand(client, message):
    whereReturn = await parsemessage.parseMessageForSQL(client, message)
    sqlStart = "SELECT channel_id as Channel, emoteName as Emote, datetimestart as 'Started at', datetimefinish as 'Ended at', total as Total, user_id as 'Started By User', ended_by as 'Ended By User', startingMessage as 'Starting Message' FROM emote_chain WHERE server_id = %s " + whereReturn["String"] + " ORDER BY total DESC " + parsemessage.workOutLimit(message)
    rows = runsql.executeSQL(sqlStart, [message.guild.id] + whereReturn['Parameters'])
    if(len(rows) == 1):
        ##if there is only 1 row of information then print it as a list
        await printStats.printList(client, message, rows)
    else:
        ##if there is more make the table less detailed but print it as a table
        for i in range(0, len(rows)):
            rows[i].pop("Started By User")
            rows[i].pop("Ended By User")
            rows[i].pop("Starting Message")
            rows[i].pop("Ended at")

        await printStats.printTable(client, message, rows)

##goes through each message since the last time it checked to see if there are any more emote chains
## it will break if someone does ?emotechain in the middle of a chain in another channel as it will say continue half way through the chain the next time, not sure how to fix
async def checkForUpdates(client, message):
    date = runsql.executeSQL("SELECT datetimefinish FROM emote_chain WHERE server_id = %s ORDER BY datetimefinish DESC LIMIT 1", [message.guild.id])
    emoteChainArray = []
    emoteChainInfo = {"startingMessage" : "", "repeatedValue" : "", "count" : 1, "channel_id": "", "startTime": None, "endTime": None, 
        "startUser": None, "endUser": None}

    rows = []
    ##depending on if a date exists grab all the messages on that server
    if(date):
        rows = runsql.executeSQL("SELECT message, channel_id, datetime, user_id FROM message WHERE server_id = %s AND datetime > %s ORDER BY channel_id, datetime", [message.guild.id, str(date[0]['datetimefinish'])])
    else:
        rows = runsql.executeSQL("SELECT message, channel_id, datetime, user_id FROM message WHERE server_id = %s ORDER BY channel_id, datetime", [message.guild.id])
    

    for x in range(0, len(rows)):
        
        row = rows[x]
        #if it is an emote chain increase the count and set the time to be the last one
        if(parsemessage.removeIDfromEmote(row['message']) == emoteChainInfo["repeatedValue"] and row['channel_id'] == emoteChainInfo['channel_id'] and not row['user_id'] == rows[x-1]['user_id']):
            emoteChainInfo["count"] = emoteChainInfo["count"] + 1
            emoteChainInfo["endTime"] = row['datetime']
        else:
            if(emoteChainInfo['count'] > 3):
                emoteChainInfo['endUser'] = row['user_id']
                emoteChainInfo['endTime'] = row['datetime']
                emoteChainArray.append(emoteChainInfo)
            ##on a successful chain then rest the information
            emoteChainInfo = { "startingMessage" : parsemessage.removeIDfromEmote(rows[x-1]['message']),
                            "repeatedValue" : parsemessage.removeIDfromEmote(row['message']),
                            "count" : 1,
                            "channel_id": row['channel_id'],
                            "startTime": row['datetime'],
                            "endTime": None, 
                            "startUser": row['user_id'],
                            "endUser": None}

    if(emoteChainInfo['count'] > 3):
        emoteChainInfo['endUser'] = row['user_id']
        emoteChainInfo['endTime'] = row['datetime']
        emoteChainArray.append(emoteChainInfo)

    ##for all the emote chains found then add it to the database
    for info in emoteChainArray:
        runsql.executeSQL("INSERT INTO emote_chain (server_id, channel_id, datetimestart, datetimefinish, total, emoteName, user_id, ended_by, startingMessage) " +
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", [message.guild.id, info['channel_id'], info['startTime'], info['endTime'], 
                info['count'], info['repeatedValue'], info['startUser'], info['endUser'], info['startingMessage']])

    await runCommand(client, message)
