import parsemessage
import runsql
import printStats

async def EmoteStats(client, message):
    ##get information based on what the user typed
    sqlMessageParse = await parsemessage.parseMessageForSQL(client, message)

    ##based on what information the user has given narrow down the search to show less options
    userFound = sqlMessageParse['valuesFound']['user']
    emoteFound = sqlMessageParse['valuesFound']['emote']
    channelFound = sqlMessageParse['valuesFound']['channel']

    ##add these titles in depending on how narrow the search is
    
    sqlTitles = {'User':"user_id as User,",
                'Channel': "channel_id as Channel,",
                'Emote': "emoteName as Emote,"}

    if(not userFound and not emoteFound and not channelFound):
        await client.send_message(message.channel, "Please mention a emote, user, channel or role")
        return

    ##add the information found earlier from sqlMessagePrase
    startingSQL = "SUM(count) as Total FROM emote WHERE server_id = %s " + sqlMessageParse['String']
    ##limit the search based on the information provided in the message
    endstring = " ORDER BY total DESC " + parsemessage.workOutLimit(message)

    ##create the sql strings to get the appropriate information
    sqlStrings = {'User': "SELECT " + sqlTitles['User'] + startingSQL + " GROUP BY user_id " + endstring,
                'Channel': "SELECT " + sqlTitles['Channel'] + startingSQL + " GROUP BY channel_id " + endstring,
                'Emote': "SELECT " + sqlTitles['Emote'] + startingSQL + " GROUP BY emoteName " + endstring}

    ##special case for if all 3 are mentioned
    if(userFound and emoteFound and channelFound):
        rows = runsql.executeSQL(sqlStrings["User"], [message.server.id] + sqlMessageParse["Parameters"])
        return

    for key in sqlStrings:
        if(not sqlStrings[key] == ""):
            await printStats.printTable(client, message, runsql.executeSQL(sqlStrings[key], [message.server.id] + sqlMessageParse["Parameters"]))