import parsemessage
import runsql
import printStats

async def emoteAllStats(message):
    groupBy = {"emote":"EmoteName", "user": "user_id","channel":"channel_id"}
    sqlSelector = {"emote":"emoteName as Emote, ","user":"user_id as User, ","channel":"channel_id as Channel, "}

    ##find out which catergory is mentioned in the message
    if('emote' in message.content or 'user' in message.content or 'channel' in message.content):
        if(not 'emote' in message.content):
            del groupBy['emote']
        if(not 'user' in message.content):
            del groupBy['user']
        if(not 'channel' in message.content):
            del groupBy['channel']

    ##get information from the string sent
    whereReturn = await parsemessage.parseMessageForSQL(message, users = True, emotes = True, channels = True, roles = True)
    for key in groupBy:
        ##combine an sql statement based on what users have been selected
        sqlStart = "SELECT " + sqlSelector[key] + " SUM(count) as Total FROM emote WHERE server_id = %s " + whereReturn["String"] + " GROUP BY " + groupBy[key]
        if " asc " in message.content.lower() + " ":
            sqlStart += " ORDER BY total ASC "
        else:
            sqlStart += " ORDER BY total DESC "
        limit = parsemessage.workOutLimit(message, value=True)
        sqlStart += " LIMIT " + str(limit)
        parameters = [message.guild.id] + whereReturn['Parameters']
        if(key == "emote"):
            emoteList = parsemessage.combineEmotes(runsql.executeSQL(sqlStart, parameters))
            if len(emoteList) < limit:
                for serverEmoji in message.guild.emojis:
                    for emote in emoteList:
                        #print(serverEmoji.name, emote["Emote"])
                        if serverEmoji.name.lower() == emote["Emote"].lower():
                            break
                    else:
                        emoteList.append({"Emote": serverEmoji.name, "Total": 0})
                    if len(emoteList) >= limit:
                        break

            await printStats.printTable(message, emoteList)
        else:
            await printStats.printTable(message, runsql.executeSQL(sqlStart, parameters))

