from lxml import html
import os
import requests

os.remove("DongerList.txt")
dongerList = open("DongerList.txt", "a", encoding='utf-8')

for x in range (1,41):
    page = requests.get("http://dongerlist.com/page/" + str(x))
    tree = html.fromstring(page.content)

    dongers = tree.xpath('//textarea[@class="donger"]/text()');
    for donger in dongers:
        dongerList.write(str(donger) + "\n")
dongerList.close()

