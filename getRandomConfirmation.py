import random
async def getRandomConfirmation(message):
    confirmations = [ "It is certain",
                        "It is decidedly so",
                        "Without a doubt",
                        "Yes definitely",
                        "You may rely on it",
                        "As I see it, yes",
                        "Most likely",
                        "Only slightly likely",
                        "Yes",
                        "Your Mum",
                        "Unlikely",
                        "Signs point to no",
                        "Don't count on it",
                        "My reply is no",
                        "My sources say no",
                        "Not good",
                        "Computer Says No",
                        "Very doubtful"]
    await message.channel.send(random.choice(confirmations))