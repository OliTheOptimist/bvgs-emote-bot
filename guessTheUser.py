import runsql
import random
import datetime
import parsemessage
import regex

currentGuessGames = []

async def startGame(message):
	if(any(x for x in currentGuessGames if x["channel"] == message.channel)):
		await message.channel.send("A Guess the User game is already in progress >:(")
		return

	randomMessageInfo = None
	for x in range(10):
		potentialMessages = runsql.executeSQL("""
			SELECT message.user_id, message.message, message.datetime, message.channel_id FROM
				(SELECT user_id FROM
					(SELECT user_id, count(message) as TotalMessages
					FROM message
					WHERE server_id = %s
					AND datetime >= CURDATE() - INTERVAL 2 WEEK
					GROUP BY user_id)
				as potential_users
				WHERE TotalMessages > 60
				ORDER BY Rand() LIMIT 1)
			as randomUser
			INNER JOIN message on randomUser.user_id = message.user_id
			WHERE message.datetime >= CURDATE() - INTERVAL 20 WEEK
			AND LENGTH(message.message) > 30 ORDER BY Rand() LIMIT 1000""",[[message.guild.id]])
		if len(potentialMessages) == 0:
			await message.channel.send("There is not enough data to run this command")
			return
		user = await message.guild.fetch_member(potentialMessages[0]['user_id'])
		if user.bot:
			continue
		for potentialMessage in potentialMessages:
			if parsemessage.countMessageLengthIgnoringTags(potentialMessage["message"]) > 20:
				potentialMessage["message"] = parsemessage.replaceIDsWithTags(message, potentialMessage["message"])
				randomMessageInfo = potentialMessage
				break
		if randomMessageInfo != None:
			break
	else:
		print("Couldn't find a suitable user")
		return
	user = await message.guild.fetch_member(randomMessageInfo["user_id"])
	channel = message.guild.get_channel(randomMessageInfo["channel_id"]).mention
	currentGuessGames.append({"channel": message.channel, "message": randomMessageInfo['message'], "correctUser":user, "endTime":datetime.datetime.now() + datetime.timedelta(seconds = 60),"postedIn": channel, "postedWhen": randomMessageInfo["datetime"], "currentGuesses":[]})
	await message.channel.send("**Guess who said this message with the command '?guess Username'. You have 1 minute!**\n" + randomMessageInfo['message'])

async def makeGuess(message):
	if len(message.mentions) > 0 or len(message.role_mentions) > 0:
		await message.channel.send("Please do not tag any users or roles, instead type their name")
		return
	for x in range(0, len(currentGuessGames)):
		if(currentGuessGames[x]["channel"] == message.channel):
			guess = parsemessage.removeCommand(message.content).strip()
			guessedUser = None
			for user in message.guild.members:
				if guess.lower() == user.display_name.strip().lower() or guess.lower() == user.name.lower():
					guessedUser = user
					break
			if guessedUser == None:
				await message.channel.send("User " + guess + " not found!")
				return

			userFound = False
			for y in range(0, len(currentGuessGames[x]["currentGuesses"])):
				if(currentGuessGames[x]["currentGuesses"][y]["user_id"] == message.author.id):
					currentGuessGames[x]["currentGuesses"][y]["guess"] = guess
					await message.channel.send(message.author.display_name + " has updated their guess to " + guessedUser.display_name)
					userFound = True
			if not userFound:
				currentGuessGames[x]["currentGuesses"].append({"user_id": message.author.id,"user": message.author.display_name, "guess": guessedUser.id})
				await message.channel.send(message.author.display_name + " has made a guess for " + guessedUser.display_name)
			return
	await message.channel.send("No Guess the User games in progress")

async def checkGuess():
	for x in range(0,len(currentGuessGames)):
		if currentGuessGames[x]["endTime"] < datetime.datetime.now():
			printOutString = "For the guess: " + currentGuessGames[x]["message"] + "\n"
			printOutString += "The correct answer was " + currentGuessGames[x]["correctUser"].display_name + "\n"
			printOutString += "Channel: " + currentGuessGames[x]["postedIn"] + "\n"
			printOutString += "Date: " + str(currentGuessGames[x]["postedWhen"]) + "\n"
			listOfWinners = []
			for y in range(0,len(currentGuessGames[x]["currentGuesses"])):
				if currentGuessGames[x]["correctUser"].id == currentGuessGames[x]["currentGuesses"][y]["guess"]:
					listOfWinners.append(currentGuessGames[x]["currentGuesses"][y]["user"])
			if len(listOfWinners) == 0:
				printOutString += "No one won"
			else:
				printOutString += "These are the winners: " + ",".join(listOfWinners)
			await currentGuessGames[x]["channel"].send(printOutString)
			del currentGuessGames[x]

	