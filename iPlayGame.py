import runsql
import parsemessage
import discordBotFunctions
import emoji
import regex
import asyncio
import discord

class iPlayGame:
    currentCommands = []

    ##dedicates a channel where the gametaggin is setup
    async def setUpRoleGroup(self, message):
        if(not await discordBotFunctions.checkUser(message)):
            return

        if message.content.strip() == "":
            await message.channel.send("Please give the role group a name after the command", delete_after=5)
            await message.delete(delay=5)
            return

        row = runsql.executeSQL("SELECT group_name FROM role_tagger_group WHERE server_id = %s and upper(group_name) = %s", [message.guild.id,message.content.upper()])
        if(row):
            await message.channel.send("A role group with that name has already been created, please pick a unique name",delete_after=5)
            return

        embedVar = discord.Embed(title=message.content, color=0x79dce8)

        newRoleMessage = await message.channel.send(embed=embedVar)
        actionMessage = await message.channel.send("Add a description")
        
        sql = "INSERT INTO role_tagger_group (server_id, channel_id, group_name, message_id) VALUES (%s, %s, %s, %s)"
        newCreatedGroupId = runsql.insertAndGetID(sql, [message.guild.id, message.channel.id, message.content, newRoleMessage.id])
        self.currentCommands.append({
            "channel":message.channel.id,
            "user": message.author.id,
            "embedMessage": newRoleMessage,
            "actionMessage":actionMessage,
            "action": "setdescription",
            "role_group_id": newCreatedGroupId,
            "group_name":message.content,
            "creation": True})
        await message.delete()  
        return

    async def editRoleGroup(self,message):
        if(not await discordBotFunctions.checkUser(message)):
           return
        await message.delete(delay=5)
        row = runsql.executeSingleSQL("SELECT id, channel_id, message_id, group_name,taggable FROM role_tagger_group WHERE server_id = %s and upper(group_name) = %s", [message.guild.id,message.content.upper()])
        if row:
            channel = message.guild.get_channel(row["channel_id"])
            embedMessage = await channel.fetch_message(row["message_id"])
            actionMessage = await message.channel.send(self.sendEditCommands(row["id"]))
            self.currentCommands.append({
                "channel":message.channel.id,
                "user": message.author.id,
                "embedMessage": embedMessage,
                "actionMessage":actionMessage,
                "role_group_id": row['id'],
                "creation": False,
                "action": ""})
            return
        else:
            await message.channel.send("There are no role groups with that name",delete_after=5)
            return

    async def getNameAndEmote(self, message, client=None, required = True):
        emote = ""
        ##adds emotes to the list to store      
        gameName = emoji.demojize(message.content)
        ##adds custom emotes as emotes
        emoteMatches = regex.findall(r"<a?:\w+:\d+>|:\w+:", gameName)
        if len(emoteMatches) > 0 and required:
            emote = emoteMatches[0]
            if "<" in emote:
                for clientEmote in client.emojis:
                    if emote == str(clientEmote):
                        break
                else:
                    await message.channel.send("I can't see this emoji, please use another", delete_after=5)
                    return False

        ##removes all custom emotes from the string
        gameName = regex.sub(r"<a?:\w+:\d+>|:\w+:", "", gameName)

        if emote == "" and required:
            await message.channel.send("You must assign each game an emote", delete_after=5)
            return False

        ##stores the remaining string as the game name
        gameName = gameName.strip()

        return {"gameName": gameName, "emote": emote}

    def sendEditCommands(self,guild_id):
        info = runsql.executeSingleSQL("SELECT group_name, taggable,confirmation_message FROM role_tagger_group WHERE id = %s", [guild_id])
        string = "```Use one of the following commands to edit '" + info["group_name"] + "':"
        if info["taggable"] == 0:
            string += "\nCurrently only admins can tag these roles"
        else:
            string += "\nCurrently all members can tag these roles"
        if info["confirmation_message"] == 1:
            string += "\nUsers are notified when they change their roles with a message at the bottom of the channel"
        else:
            string += "\nThere is no message response when the user tags themselves with a react"
        string += "\n\n?setTitle            ?addRole         ?updateRoles            ?swapTotalCount          ?removeGroup"
        string += "\n?setDescription      ?removeRole      ?orderRoles             ?swapTaggable            ?moveGroupHere"
        string += "\n?setSubTitle         ?setEmote                                ?swapMentions"
        string += "\n"
        string += "\n\n?stop - when you are finished```"
        return string

    async def checkCurrentCommands(self,client, message):
        index = -1
        for x in range(len(self.currentCommands)):
            if self.currentCommands[x]["channel"] == message.channel.id and self.currentCommands[x]["user"] == message.author.id:
                index = x
                break
        else:
            return False

        if message.content[0] == "$" or message.content[0] == "?":
            commandContent = message.content[1:].split(" ")
            self.currentCommands[x]["action"] = commandContent[0].lower()
            message.content = " ".join(commandContent[1:])
        elif not self.currentCommands[x]["creation"]:
            self.currentCommands[x]["action"] = ""

        if self.currentCommands[x]["action"] == "stop":
            await self.currentCommands[x]["actionMessage"].delete()
            del self.currentCommands[x]
        elif self.currentCommands[x]["action"] == "setdescription" or self.currentCommands[x]["action"] == "setdesc":
            self.currentCommands[x] = await self.setDescription(message, self.currentCommands[x])

        elif self.currentCommands[x]["action"] == "setsubtitle":
            self.currentCommands[x] = await self.setSubTitle(message, self.currentCommands[x])
            
        elif self.currentCommands[x]["action"] == "settitle":
            await self.setTitle(message, self.currentCommands[x])
        elif self.currentCommands[x]["action"] == "addrole" or self.currentCommands[x]["action"] == "addgame" :
            addGame = await self.addGame(client, message, self.currentCommands[x])
        elif self.currentCommands[x]["action"] == "updateroles" or self.currentCommands[x]["action"] == "updategroup":
            await self.updateGameMessageForServer(message, group_id = self.currentCommands[x]["role_group_id"])
        elif self.currentCommands[x]["action"] == "orderroles" or self.currentCommands[x]["action"] == "ordergroup":
            await self.updateGameMessageForServer(message, group_id = self.currentCommands[x]["role_group_id"], order = True)
        elif self.currentCommands[x]["action"] == "setemote":
            await self.setEmote(client, message, self.currentCommands[x]["role_group_id"])
        elif self.currentCommands[x]["action"] == "removerole":
            await self.removeGame(message, self.currentCommands[x]["role_group_id"])
        elif self.currentCommands[x]["action"] == "removegroup":
            await self.removeGroup(self.currentCommands[x])
            await self.currentCommands[x]["actionMessage"].delete()
            del self.currentCommands[x]
            await message.channel.send("Group deleted", delete_after=5)
        elif self.currentCommands[x]["action"] == "swaptaggable":
            self.currentCommands[x] = await self.swapTaggable(message, self.currentCommands[x])
        elif self.currentCommands[x]["action"] == "movegrouphere":
            self.currentCommands[x] = await self.moveGroup(message, self.currentCommands[x])
        elif self.currentCommands[x]["action"] == "swaptotalcount":
            await self.swapShowTotal(message, self.currentCommands[x])
        elif self.currentCommands[x]["action"] == "swapmentions":
            self.currentCommands[x] = await self.swapMentions(message, self.currentCommands[x])
        else:
            await message.channel.send("Command not recognised, type ?stop to editing this group", delete_after=5)
        await message.delete(delay=5)
        return True

    async def setDescription(self, message, commandDictionary):
        if message.content.strip() == "":
            await message.channel.send("Please type something after the command",delete_after=5)
            return
        embed = commandDictionary["embedMessage"].embeds[0]
        embed.description = message.content
        await commandDictionary["embedMessage"].edit(embed=embed)
        if commandDictionary["creation"]:
            await commandDictionary["actionMessage"].delete()
            actionMessage = await message.channel.send("Add a subtitle for your list:")
            commandDictionary["actionMessage"] = actionMessage
            commandDictionary["action"] = "setsubtitle"
        return commandDictionary
    
    async def moveGroup(self, message, commandDictionary):
        embed = commandDictionary["embedMessage"].embeds[0]
        await commandDictionary["embedMessage"].delete()
        await commandDictionary["actionMessage"].delete()
        newMessage = await message.channel.send(embed = embed)
        row = runsql.executeSingleSQL("SELECT group_name,taggable FROM role_tagger_group WHERE id = %s", [commandDictionary["role_group_id"]])
        newAction = await message.channel.send(self.sendEditCommands(commandDictionary["role_group_id"]))
        commandDictionary["embedMessage"] = newMessage
        runsql.executeSQL("UPDATE role_tagger_group SET message_id=%s, channel_id=%s WHERE id = %s", [newMessage.id, newMessage.channel.id, commandDictionary["role_group_id"]])
        commandDictionary["actionMessage"] = newAction
        await self.updateGameMessageForServer(message,group_id = commandDictionary["role_group_id"])
        return commandDictionary

    async def swapTaggable(self, message, commandDictionary):
        row = runsql.executeSingleSQL("SELECT taggable,group_name FROM role_tagger_group WHERE id = %s", [commandDictionary["role_group_id"]])
        newTaggable = 0
        if row["taggable"] == 0:
            newTaggable = 1
        rows = runsql.executeSQL("SELECT role_id FROM role_tags WHERE role_group_id = %s", [commandDictionary["role_group_id"]])
        for role in rows:
            if newTaggable == 0:
                await message.guild.get_role(role["role_id"]).edit(mentionable = False)
            else:
                await message.guild.get_role(role["role_id"]).edit(mentionable = True)
        runsql.executeSQL("UPDATE role_tagger_group SET taggable=%s WHERE id = %s", [newTaggable, commandDictionary["role_group_id"]])
        if newTaggable == 0:
            await message.channel.send("All roles are now only taggable by admins!", delete_after=5)
        else:
            await message.channel.send("All roles are now taggable by all members!", delete_after=5)
        await commandDictionary["actionMessage"].delete()
        actionMessage = await message.channel.send(self.sendEditCommands(commandDictionary["role_group_id"]))
        commandDictionary["actionMessage"] = actionMessage
        return commandDictionary

    async def swapShowTotal(self, message, commandDictionary):
        runsql.executeSingleSQL("UPDATE role_tagger_group SET show_total_users = !show_total_users WHERE id = %s", [commandDictionary["role_group_id"]])
        await self.updateGameMessageForServer(message, group_id = commandDictionary["role_group_id"])

    async def swapMentions(self, message, commandDictionary):
        runsql.executeSingleSQL("UPDATE role_tagger_group SET confirmation_message = !confirmation_message WHERE id = %s", [commandDictionary["role_group_id"]])
        await commandDictionary["actionMessage"].delete()
        actionMessage = await message.channel.send(self.sendEditCommands(commandDictionary["role_group_id"]))
        commandDictionary["actionMessage"] = actionMessage
        return commandDictionary
        

    async def setSubTitle(self, message, commandDictionary):
        if message.content.strip() == "":
            await message.channel.send("Please type something after the command",delete_after=5)
            return
        embed = commandDictionary["embedMessage"].embeds[0]
        if len(embed.fields) == 0:
            embed.add_field(name=message.content,value="Temp")
        else:
            embed.set_field_at(0, name=message.content, value=embed.fields[0].value)
        newEmbed = await commandDictionary["embedMessage"].edit(embed=embed)
        if commandDictionary["creation"]:
            await commandDictionary["actionMessage"].delete()
            commandDictionary["creation"] = False
            actionMessage = await message.channel.send(self.sendEditCommands(commandDictionary["role_group_id"]))
            commandDictionary["actionMessage"] = actionMessage
            commandDictionary["setup"] = False
        return commandDictionary

    async def setTitle(self, message, commandDictionary):
        if message.content.strip() == "":
            await message.channel.send("Please type something after the command",delete_after=5)
            return
        embed = commandDictionary["embedMessage"].embeds[0]
        embed.title = message.content
        await commandDictionary["embedMessage"].edit(embed=embed)
        runsql.executeSQL("UPDATE role_tagger_group SET group_name=%s WHERE id = %s", [message.content, commandDictionary["role_group_id"]]) 

    ##adds a game to game tagging
    async def addGame(self, client, message, commandDictionary):
        nae = await self.getNameAndEmote(message, client = client)
        if not nae:
            return
        gameName = nae['gameName']
        emote = nae['emote']
        message.content = gameName
        
        emote_already_exist = runsql.executeSingleSQL("SELECT 1 FROM role_tags WHERE role_group_id = %s AND emote = %s", [commandDictionary["role_group_id"], emote])
        if(emote_already_exist):
            await message.channel.send("This emote is already in use for this role group", delete_after=5)
            return False
        max_limit_reached = runsql.executeSingleSQL("SELECT count(1) as 'total_tags' FROM role_tags WHERE role_group_id = %s", [commandDictionary["role_group_id"]])
        if max_limit_reached["total_tags"] >= 20:
            await message.channel.send("This group already has the max number of roles assigned to it (20). Try creating another group for this role or remove a role from here.", delete_after=5)
            return False

        roles = parsemessage.getRoles(message, getRole = True, allowFullMessage = True)
        role = None
        if(len(roles) > 0):
            role = roles[0]
            ##checks to see if the game is already added
            role_group_id = runsql.executeSingleSQL("SELECT 1 FROM role_tags WHERE role_id = %s AND role_group_id = %s", [role.id, commandDictionary["role_group_id"]])
            if(role_group_id):
                await message.channel.send(role.name + " is already added", delete_after=5)
                return False

            row = runsql.executeSingleSQL("SELECT taggable FROM role_tagger_group WHERE id = %s", [commandDictionary["role_group_id"]])
            if row["taggable"] == 1:
                await role.edit(mentionable = True)
            else:
                await role.edit(mentionable = False)
            ##adds the game based on the id of the role mentioned
        else:
            ##creates a new role
            role = await message.guild.create_role(name = gameName, mentionable = True)
        ##stores it in the database
        runsql.executeSQL("INSERT INTO role_tags (role_group_id, role_id, emote,last_edited) VALUES (%s, %s, %s,NOW())", [commandDictionary["role_group_id"], role.id, emote])
        await message.channel.send("Added " + gameName + " to list of avaliable games", delete_after=5)
        await self.updateGameMessageForServer(message,group_id = commandDictionary["role_group_id"], order=True)
        return True

    async def updateGameMessageForServer(self, message, group_id = 0,order = False):
        if group_id == 0:
            allMessages = runsql.executeSQL("SELECT id,channel_id,message_id,show_total_users FROM role_tagger_group WHERE server_id = %s", [message.guild.id])
        else:
            allMessages = runsql.executeSQL("SELECT id,channel_id,message_id,show_total_users FROM role_tagger_group WHERE id = %s", [group_id])
        for roleMessage in allMessages:

            messageString = ""
            roleGroupRoles = runsql.executeSQL("SELECT id,role_id,emote FROM role_tags WHERE role_group_id = %s ORDER BY last_edited ASC", [roleMessage["id"]])
            roleMessageObject = await message.guild.get_channel(roleMessage["channel_id"]).fetch_message(roleMessage["message_id"])
            if not roleGroupRoles:
                break

            if order:
                for x in range(len(roleGroupRoles)):
                    roleGroupRoles[x]["role_name"] = message.guild.get_role(roleGroupRoles[x]["role_id"]).name
                roleGroupRoles = sorted(roleGroupRoles, key=lambda x: x["role_name"].lower())
                await roleMessageObject.clear_reactions()
                roleMessageObject.reactions = []

            validReacts = []
            FieldsToAdd = []
            for role_info in roleGroupRoles:
                role = message.guild.get_role(role_info["role_id"])
                if not role:
                    runsql.executeSQL("DELETE FROM role_tags WHERE role_id = %s", [role_info["role_id"]])
                    continue
                if order:
                   runsql.executeSQL("UPDATE role_tags SET last_edited = NOW(3) WHERE id = %s", [role_info["id"]]) 
                currentString = emoji.emojize(role_info["emote"]) + "  "
                currentString += role.name
                if roleMessage["show_total_users"] == 1:
                    currentString += "   (" + str(len(role.members)) + ")"
                currentString += "\n"
                validReacts.append(role_info["emote"])
                #try:
                
                if len(messageString) + len(currentString) > 1024:
                    FieldsToAdd.append(messageString)
                    messageString = currentString
                else:
                    messageString += currentString
                #except Exception as e:
                #    await message.channel.send("The emoji for "+ role.name + " doesn't exist anymore, please re-add the role to the group", delete_after=5)
                #    runsql.executeSQL("DELETE FROM role_tags WHERE role_id = %s AND role_group_id = %s", [role_info["role_id",roleMessage["id"]]])
            
            #await roleMessageObject.add_reaction(emoji.emojize(role_info["emote"]))
            FieldsToAdd.append(messageString)

            #print("--------------NEW------------------")
            for react in roleMessageObject.reactions:
            #    print("Looking at ", emoji.demojize(str(react.emoji)))
                if emoji.demojize(str(react.emoji)) in validReacts:
                    continue
                else:
                    await react.clear()
            #        print("Emote Removed")

            
            for reactEmote in validReacts:
            #    print("Looking at ", reactEmote)
                found = False
                for react in roleMessageObject.reactions:
            #        print(emoji.demojize(str(react.emoji)), reactEmote)
                    if emoji.demojize(str(react.emoji)) == reactEmote:
                        found = True
                        break
                if found == False:
            #        print("Emote Added")
                    await roleMessageObject.add_reaction(emoji.emojize(reactEmote))

            embed = roleMessageObject.embeds[0]
            subtitleName = embed.fields[0].name
            embed.clear_fields()
            embed.add_field(name=subtitleName, value=FieldsToAdd[0])
            for x in range(1,len(FieldsToAdd)):
                if FieldsToAdd[x] != "":
                    embed.add_field(name="⠀",value=FieldsToAdd[x])
            await roleMessageObject.edit(embed=embed)

    ##adds a game to game tagging
    async def setEmote(self,client, message, role_group_id):           
        nae = await self.getNameAndEmote(message, client=client)
        if not nae:
            return
        emote = nae['emote']
        emote_in_use = runsql.executeSingleSQL("SELECT 1 FROM role_tags WHERE role_group_id = %s AND emote = %s", [role_group_id, emote])
        if(emote_in_use):
            await message.channel.send("This emote is already in use for this role group", delete_after=5)
            return False
        message.content = nae['gameName']
        roles = parsemessage.getRoles(message, getRole = True, allowFullMessage = True)
        if(not roles):
            await message.channel.send("That role does not exist", delete_after=5)
            return
        for role in roles:
            runsql.executeSQL("UPDATE role_tags SET emote = %s, last_edited = NOW(3) WHERE role_id = %s", [emote, role.id])
            await message.channel.send("Emote set for " + role.name, delete_after=5)
        await self.updateGameMessageForServer(message,group_id = role_group_id, order = True)                

    ##removes games from game tagging
    async def removeGame(self, message, role_group_id):
        nae = await self.getNameAndEmote(message, required = False)
        message.content = nae["gameName"]
        roles = parsemessage.getRoles(message, getRole = True, allowFullMessage = True)
        
        if(not roles):
            await message.channel.send("No roles found to remove, please tag a role to remove it")
            return
        
        ##delete all roles found in the message
        for role in roles:
            ##have to save the name because the role is deleted
            roleName = role.name
            runsql.executeSQL("DELETE FROM role_tags WHERE role_group_id = %s AND role_id = %s", [role_group_id, role.id])
            #await role.delete()
            await message.channel.send(roleName + " removed from the list of taggable games", delete_after=5)

        ##updates the list of avaliable games to tag
        await self.updateGameMessageForServer(message,group_id = role_group_id)

    async def modifyRoleUsingReaction(self, client, reaction, actionType):
        role_group_id = runsql.executeSingleSQL("SELECT id, show_total_users,confirmation_message FROM role_tagger_group WHERE server_id = %s AND message_id = %s", [reaction.guild_id,reaction.message_id])
        if not role_group_id:
            return

        role_id = runsql.executeSingleSQL("SELECT role_id FROM role_tags WHERE role_group_id = %s and emote = %s", [role_group_id["id"], emoji.demojize(str(reaction.emoji))])
        if not role_id:
            return
        guild = client.get_guild(reaction.guild_id)
        channel = guild.get_channel(reaction.channel_id)
        message = await channel.fetch_message(reaction.message_id)
        role = guild.get_role(role_id["role_id"])
        member = guild.get_member(reaction.user_id)
        action = "added"
        if role in member.roles:
            action = "removed"
            await member.remove_roles(role)
        else:
            await member.add_roles(role)

        for reaction in message.reactions:
            async for user in reaction.users():
                if user.id == member.id:
                   await reaction.remove(user)
        if role_group_id["show_total_users"] == 1:
            await self.updateGameMessageForServer(message,group_id = role_group_id["id"])
        if role_group_id["confirmation_message"] == 1:
            if action == "added":
                await channel.send("The role " + role.name + " has been added to " + member.display_name,delete_after=10)
            else:
                await channel.send("The role " + role.name + " has been removed from " + member.display_name,delete_after=10)


        

    ##adds a game to game tagging
    async def removeGroup(self, commandDictionary):           
        await commandDictionary["embedMessage"].delete()
        runsql.executeSQL("DELETE FROM role_tags WHERE role_group_id = %s", [commandDictionary["role_group_id"]])
        runsql.executeSQL("DELETE FROM role_tagger_group WHERE id = %s", [commandDictionary["role_group_id"]])

        ##starts the messageString

        # roles = discordBotFunctions.getRolesFromIds(message, rows)
        # # x = 0
        # # while x < len(roles):
        # #     if x == 0:
        # #         maxPos = 0
        # #         for role in roles:
        # #             if role.position > maxPos:
        # #                 maxPos = role.position
        # #         if(roles[x] != role.position):
        # #             await roles[x].edit(position = maxPos)
        # #             roles = discordBotFunctions.getRolesFromIds(message, rows)
        # #             x = 0
        # #     elif roles[x].position > roles[x-1].position:
        # #         await roles[x].edit(position = roles[x-1].position)
        # #         roles = discordBotFunctions.getRolesFromIds(message, rows)
        # #         x = 0
        # #     x += 1 
        # for role in roles:
        #     emote = runsql.executeSingleSQL("SELECT emote FROM possible_games WHERE server_id = %s AND role_id = %s", [message.guild.id, role.id])   
        #     messageString += "?iplay " + role.name + " " + emoji.emojize(emote['emote']) + "\n"

        # messageString += "\nIf you play a game that is not on the list then ask the admins to add it"

        # row = runsql.executeSingleSQL("SELECT * FROM game_tagger_location WHERE server_id = %s",[message.guild.id])
        # channel = message.guild.get_channel(row['channel_id'])
        # if(not channel):
        #     await channel.send("Can't find flaring-games channel, please use ?startgametagginghere to choose a channel")
        #     return
        # ##edits the message posted when the setupgametagging was created
        # flaringMessage = await channel.fetch_message(row['message_id'])
        # await flaringMessage.edit(content=messageString)
        # if(fromCommand):
        #     await message.channel.send("Game tagging message has been updated",delete_after=5)

    
    

    


    # ##disables game tagging
    # async def stopGameHere(self, message):
    #     if(not await discordBotFunctions.checkUser(message)):
    #         return

    #     row = runsql.executeSQL("SELECT channel_id, message_id FROM game_tagger_location WHERE server_id = %s", [message.guild.id])
    #     if(not row):
    #         await message.channel.send("User tagging is not set up")
    #         return

    #     ##removes channel so people can post in there again
    #     self.gameChannels.remove(row[0]['channel_id'])
    #     channel = message.guild.get_channel(row[0]['channel_id'])
    #     if(channel):
    #         messageDelete = await channel.fetch_message(row[0]['message_id'])
    #         if(messageDelete):
    #             ##deletes the message that the bot has to display the games avaliable
    #             await messageDelete.delete()

    #     runsql.executeSQL("DELETE FROM game_tagger_location WHERE server_id = %s", [message.guild.id])
    #     await message.channel.send("Game tagging has been stopped", delete_after=5)
    #     await message.delete(delay=5)







    # ##checks to see if the message posted is in the game channel, and if it is then it is deleted
    # async def checkIfPlayChannel(self,message):
    #     commands = [self.startingChar + "iplay ", self.startingChar + "addgame ", self.startingChar + "removegame ", self.startingChar + "setupgametagginghere", self.startingChar + "stopgametagging", self.startingChar + "setemote ", self.startingChar + "updategametags ", self.startingChar + "delete "]
    #     if(any(message.content.startswith(command) for command in commands)):
    #         return False
    #     if(message.channel.id in self.gameChannels):
    #         await message.channel.send("Excuse me, posts are not allowed in this channel", delete_after=5)
    #         await message.delete()
    #         return True
    #     return False

    

    



                
