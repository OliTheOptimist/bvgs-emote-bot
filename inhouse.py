import emoji
import re
import datetime

class inhouse:
	currentInhouse = []

	def loadInhouseData(self):
		pass

	async def startInhouse(self,message):
		print(emoji.demojize(message.content))
		if len(message.role_mentions) != 1:
			await message.channel.send("Make sure to tag only one game", delete_after=5)
			return
		gameRole = message.role_mentions[0]
		await message.add_reaction(emoji.emojize(":white_check_mark:", use_aliases=True))
		message_start = emoji.emojize(message.author.display_name + " has started an inhouse ping, react with a :white_check_mark: if you want to play")
		updateMessage = await message.channel.send(message_start)
		self.currentInhouse.append({"original_id":message.id, "emote_message": updateMessage, "message_start": message_start})
		# time = re.search(r"(\d)(:|\.)(\d+)", message.content)
		# hour = int(time.group(1))
		# minute = int(time.group(3))

		# today = datetime.today()
		# d = datetime.datetime(today.year, 11, 28, 23, 55, 59, 342380)
		# print(d)

	async def updateMessage(self, reaction,user):
		if user.bot:
			return
		print(self.currentInhouse)
		for inhouseInfo in self.currentInhouse:
			if reaction.message.id == inhouseInfo["original_id"]:
				print(reaction.emoji,emoji.emojize(":white_check_mark:", use_aliases=True))
				if reaction.emoji != emoji.emojize(":white_check_mark:", use_aliases=True):
					return
				print("reached")
				userReacts = await reaction.users().flatten()
				userString = "\nCurrent Players:\n"
				players = []
				for user in userReacts:
					players.append(user.display_name)

				await inhouseInfo["emote_message"].edit(content=inhouseInfo["message_start"] + ",".join(userString))
				return

