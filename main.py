import discord
import asyncio
import donger
import runsql #mysqlclient
import regex
import randomEmote
import emoji
import parsemessage
import printStats
import emoteallstats
import addRole
import asyncio
import threading
import iPlayGame
import wordCloudBot
import markovChain
import emoteChain
import randomUser
import waitWhat
import datetime
import getRandomConfirmation
import allmessagestats
import weeklyUsage
#import userTagging
import role
import prediction
import sinbin
import userstats
import deleteMessages
import randomUser
import guessTheUser
import addValidChannels
import inhouse
import logging

#pip install discord
#pip install regex
#pip install emoji
#pip install python-dateutil
#pip install wordcloud
#pip install markovify
#pip install mysqlclient
#pip install matplotlib
#(sudo apt-get install python3-dev libmysqlclient-dev)

testing = False
startingChar = '?'
if(testing):
    startingChar = '$' 

intents = discord.Intents.default()
intents.members = True
client = discord.Client(intents=intents)

dongerClass = donger.Donger()
iplay = iPlayGame.iPlayGame()
#iplay.iPlayGameLoad(startingChar)
#userTagging = userTagging.userTagging()@
#userTagging.userTaggingLoad(startingChar)
validChannels = addValidChannels.addValidChannels()
validChannels.loadValidChannels()
inhouseObj = inhouse.inhouse()



async def runCommand(client, message):
    commandDictionary = {
        "donger": {"command": dongerClass.runDonger, "tags": ["donger","random"]},
        #"addrole": {"command": addRole.addRole, "tags": ["add","role"]},
        "emotestats": {"command": emoteallstats.emoteAllStats, "tags": ["emote","stats"]},
        #"iplay": {"command": iplay.iPlayGameMain, "tags": ["play"]},
        "editrolegroup": {"command": iplay.editRoleGroup, "tags": ["add","game"]},
        #"removegame": {"command": iplay.removeGame, "tags": ["remove","game"]},
        #"updategametags": {"command": iplay.updateGameMessageForServer, "tags": ["update", "game", "tags"]},
        "createrolegroup": {"command": iplay.setUpRoleGroup, "tags": ["set","game", "tagging", "here"]},
        #"stopgametagging": {"command": iplay.stopGameHere, "tags": ["stop","game", "tagging"]},
        #"setemote": {"command": iplay.setEmote, "tags": ["set","emote"]},
        "wordcloud": {"command": wordCloudBot.createWordCloud, "tags": ["wordcloud"]},
        "markov": {"command": markovChain.markovChain, "tags": ["markov"]},
        "emotechain": {"command": emoteChain.checkForUpdates, "tags": ["emote","chain"]},
        "waitwhat": {"command": waitWhat.runHelpCommands, "tags": ["help", "commands"]},
        "messagestats": {"command": allmessagestats.messageAllStats, "tags": ["message","stats"]},
        "weeklystats": {"command": weeklyUsage.getWeeklyUsage, "tags": ["weekly","stats"]},
        #"setupusertagginghere": {"command": userTagging.setUpUserTaggingHere, "tags": ["setup","user", "tagging"]},
        #"stopusertagging": {"command": userTagging.stopUserTaggingHere, "tags": ["stop","user", "tagging"]},
        #"addtag": {"command": userTagging.addNormalRole, "tags": ["add","user", "tag"]},
        #"ignoretag": {"command": userTagging.addIgnoreRole, "tags": ["ignore", "tag"]},
        #"removetag": {"command": userTagging.removeRole, "tags": ["remove","tag"]},
        #"im": {"command": userTagging.roleTaggingMain, "tags": ["im"]},
        #"settag": {"command": userTagging.setDefinition, "tags": ["set","explanation"]},
        #"updateusertags": {"command": userTagging.updateRoleMessageForServer, "tags": ["update", "user", "tags"]},
        "rolestats": {"command": role.roleStats, "tags": [["role","stats"], ["how", "many", "play"]]},
        "createprediction": {"command": prediction.createPrediction, "tags": ["create","prediction"]},
        "prediction": {"command": prediction.currentPredictions, "tags": ["view","prediction"]},
        "predict": {"command": prediction.votePrediction, "tags": ["predict"]},
        "closeprediction": {"command": prediction.stopPrediction, "tags": ["close","prediction"]},
        "openprediction": {"command": prediction.openPrediction, "tags": ["open","prediction"]},
        "setwinner": {"command": prediction.predictionWinner, "tags": ["set","winner"]},
        "predictionold": {"command": prediction.oldPredictions, "tags": ["predictions","old"]},
        "predictiontag": {"command": prediction.setPredictionTag, "tags": ["set", "prediction","tag"]},
        "sinbin": {"command": sinbin.addSinBin, "tags": [["sin","bin"], ["sinbin"]]},
        "heart": {"command": sinbin.addHeart, "tags": ["heart"]},
        "userstats": {"command": userstats.userStats, "tags": ["user","stats"]},
        "delete": {"command": deleteMessages.deleteMessages, "tags": ["delete","last","messages"]},
        "guesstheuser": {"command": guessTheUser.startGame, "tags": ["guess","user","game"]},
        "guess": {"command": guessTheUser.makeGuess, "tags": ["make","guess"]},
        "addchannel": {"command": validChannels.addChannel, "tags": ["add", "channel"]},
        "removechannel": {"command": validChannels.removeChannel, "tags": ["add", "channel"]},
        "validchannels": {"command": validChannels.showValidChannels, "tags": ["show","valid","channel"]},
        "inhouse": {"command": inhouseObj.startInhouse,"tags": ["inhouse"]}
    }

    if not message.content.lower().startswith(startingChar) or message.content.lower().startswith(startingChar*2):
        return False

    if message.content.startswith("?!") and str(message.author.id) == 126087047260667904:
        tempMessage = message.content[2:]
        await message.channel.send(tempMessage)
        return True

    command = message.content.split(" ")[0][1:].lower()
    if command in commandDictionary:
        message.content = parsemessage.removeCommand(message.content).strip()
        await commandDictionary[command]["command"](message)
    # else:
    #     message.content = message.content[1:].strip()
    #     if(message.content != ""):
    #         await searchFile.giveMessageCipher(client,message, commandDictionary)
    return True
    #except Exception as e:
    #    print(e)
    #    client.get_user(126087047260667904).send(e)
    #    return True
    
#checks the messages of those sent and edited to see if commands needed to be figured out
async def checkMessages(message, tagging = True, executeCommands = True):
    if(message.author.id == client.user.id):
        return
    if(await iplay.checkCurrentCommands(client, message)):
        return

    # if(await iplay.checkIfPlayChannel(message)):
    #     return

    # if(await userTagging.checkIfTagChannel(message)):
    #     return

    #if the user is mentioned in a post @EmoteBot
    if client.user.mentioned_in(message) and not message.mention_everyone and tagging and executeCommands:
        userQuestions = ["who", "user", "whomst", "whom"]
        yesNoQuestions = ["can", "would", "will", "has", "are", "does", "is", "should", "do"]
        if any(question in message.content.lower() for question in userQuestions):
            await randomUser.getRandomUser(message)
        elif any(yn in message.content.lower() for yn in yesNoQuestions):
            await getRandomConfirmation.getRandomConfirmation(message)
        else:
            await randomEmote.getRandomEmote(message)
        return

    if("?countdown" in message.content):
        d1 = datetime.datetime(2019, 8, 15)
        d2 = datetime.datetime.now()
        seconds = str(int(abs(d2 - d1).total_seconds())) + " seconds"
        minutes = str(int(abs(d2 - d1).total_seconds()/60)) + " minutes"
        hours = str(int(abs(d2 - d1).total_seconds()/3600)) + " hours"
        days = str(int(abs(d2 - d1).days)) + " days"
        await client.send_message(message.channel, "Count down to XP reset day: " + seconds + " or " + minutes + " or " + hours + " or " + days)
        return

    if(executeCommands):
        if(await runCommand(client,message)):
            return
    if not validChannels.isValidChannel(message.channel.id):
        return

    ##adds the message to the databasemysqlclient-1.3.9-cp35-cp35m-win_amd64.whl
    message_id = runsql.insertAndGetID("INSERT INTO message (user_id, message_id, message, channel_id, server_id, datetime) VALUES (%s, %s, %s, %s, %s, %s)",
        [message.author.id, message.id, emoji.demojize(message.content), message.channel.id, message.guild.id, message.created_at.strftime('%Y-%m-%d %H:%M:%S')])

    #adds emotes to the emote database by searching for them by <:emotename:324234234234> or by unicode
    emoteMatches = regex.findall(r"(<:(\w+):\d+>)(?!.*\1)", message.content)
    for emote in emoteMatches:
        runsql.executeSQL("INSERT INTO emote (user_id, emoteName, datetime, message_id, channel_id, server_id, count) VALUES (%s, %s, %s, %s, %s, %s, %s)",
        [message.author.id, emote[1], message.created_at, message.id, message.channel.id, message.guild.id, message.content.count(emote[1])])

    for emote in emoji.UNICODE_EMOJI:
        if(emote in message.content):
            message_id = runsql.insertAndGetID("INSERT INTO emote (user_id, emoteName, datetime, message_id, channel_id, server_id, count) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                [message.author.id, emoji.demojize(emote).replace(":", ""), message.created_at, message.id, message.channel.id, message.guild.id, message.content.count(emote)])

#when the bot loads print some stuff out and set the game
@client.event
async def on_ready():
    print('------')
    print('I have logged in as {0.user}'.format(client))
    print('------')
    await client.change_presence(status=discord.Status.online, activity=discord.Game("type ?waitwhat"))
    return
    for server in client.servers:
        for channel in server.channels:
            async for log in client.logs_from(channel, limit= 10000, after = datetime.datetime(2017, 7, 1,0,0,0)):
                exists = runsql.executeSQL("SELECT 1 FROM message WHERE message_id = %s", [log.id])
                if(not exists and not log.content.strip().startswith("?")):
                    await checkMessages(log, executeCommands = False)       
                
#on message recieved
@client.event
async def on_message(message):
    try:
        await checkMessages(message)
    except Exception as e:
        logging.exception("message")
        #await client.get_user(126087047260667904).send(str(e))


@client.event
async def on_raw_reaction_add(payload):
    if payload.user_id != client.user.id:
        await iplay.modifyRoleUsingReaction(client, payload, "add")
    
    #await inhouseObj.updateMessage(reaction,user)

# @client.event
# async def on_raw_reaction_remove(payload):
#     print(payload.user_id)
#     await iplay.modifyRoleUsingReaction(client, payload, "remove")

##when a message is edited
@client.event
async def on_message_edit(before, after):
    return
    ##delete message and emote and treat as new message
    runsql.executeSQL("DELETE FROM message WHERE message_id = %s", [before.id])
    runsql.executeSQL("DELETE FROM emote WHERE message_id = %s", [before.id])
    await checkMessages(after,tagging = False)

##on message deleted
@client.event
async def on_message_delete(message):
    runsql.executeSQL("DELETE FROM message WHERE message_id = %s", [message.id])
    runsql.executeSQL("DELETE FROM emote WHERE message_id = %s", [message.id]) 

#every 10 seconds see if there is a role that has to be removed from a user
async def my_background_task():
    await client.wait_until_ready()
    while True:
        try:
            await addRole.deleteRoles(client)
            await asyncio.sleep(10)
            await guessTheUser.checkGuess()
        except Exception as e:
            print(e)
            await client.get_user(126087047260667904).send(str(e))

     

client.loop.create_task(my_background_task())
#Test bot token
if(testing):
    client.run("MzM3MjQ1MTc4NzQ4NDY5MjQ5.XtjzWw.95MX2nKjFfOgs7OPgLjpAQ55v0M")
#Main Bot Token
else:
    client.run("MzIxNzgxMjU2MDgzNzM0NTI4.DBjJ_A.A2dhAhEkmkfCX8WWXQuKmaYouP4")