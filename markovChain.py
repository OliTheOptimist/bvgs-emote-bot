import markovify
import parsemessage
import runsql
import regex

##create a markov chain
async def markovChain(message):
    messageSQL = await parsemessage.parseMessageForSQL(message, emotes = False, dates = False)
    rows = runsql.executeSQL("SELECT message FROM message WHERE server_id = %s " + messageSQL['String'] + " LIMIT 5000", [message.guild.id] + messageSQL['Parameters'])
    text = ""

    ##check to make sure the user has over 30 messages so markov generator produces something worth while
    if(len(rows) < 30):
        await message.channel.send("There is not enough data for this command")
        return
    
    msg = await message.channel.send("Calculating...")
    for row in rows:
        messageToParse = row['message'].split("\n")
        for sentence in messageToParse:
            if parsemessage.countMessageLengthIgnoringTags(sentence) > 20:
                text += " " + sentence + "."
    # Build the model
    text_model = markovify.Text(text,state_size=2)
    ##replaces all emojis mentioned as plain text "FeelsBadMan" with proper emojis where possible
    markovString = text_model.make_short_sentence(170)
    if(markovString):
        markovString = parsemessage.replaceIDsWithTags(message, markovString)
        await msg.edit(content=markovString)
    else:
        await msg.edit(content="There is not enough data for this command")