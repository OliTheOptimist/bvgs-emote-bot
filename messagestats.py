import parsemessage
import runsql
import printStats

async def MessageStats(client, message):
    ##get information based on what the user typed
    sqlMessageParse = await parsemessage.parseMessageForSQL(client, message)

    ##based on what information the user has given narrow down the search to show less options
    userFound = sqlMessageParse['valuesFound']['user']
    channelFound = sqlMessageParse['valuesFound']['channel']

    ##add these titles in depending on how narrow the search is
    
    sqlTitles = {'User':"user_id as User,",
                'Channel': "channel_id as Channel,"}

    if(not userFound and not channelFound):
        await client.send_message(message.channel, "Please mention a user, role or channel")
        return

    ##add the information found earlier from sqlMessageParse
    startingSQL = "COUNT(id) as Total FROM message WHERE server_id = %s " + sqlMessageParse['String']
    ##limit the search based on the information provided in the message
    endstring = " ORDER BY total DESC " + parsemessage.workOutLimit(message)

    ##create the sql strings to get the appropriate information
    sqlStrings = {'User': "SELECT " + sqlTitles['User'] + startingSQL + " GROUP BY user_id " + endstring,
                'Channel': "SELECT " + sqlTitles['Channel'] + startingSQL + " GROUP BY channel_id " + endstring}

    ##special case for if all 2 are mentioned
    if(userFound and channelFound):
        rows = runsql.executeSQL(sqlStrings["User"], [message.server.id] + sqlMessageParse["Parameters"])
        return

    for key in sqlStrings:
        if(not sqlStrings[key] == ""):
            await printStats.printTable(client, message, runsql.executeSQL(sqlStrings[key], [message.server.id] + sqlMessageParse["Parameters"]))