import regex
import emoji
import datetime
import time
from dateutil.relativedelta import relativedelta

##looks at user messages and turns that information in sql terminology
async def parseMessageForSQL(message, emotes = True, channels = True, users = True, roles = True, dates = True, roleIDs = False):
    #sql params
    parameters = []
    SQLString = []

    ##arrays are used as they are joined together at the end using .join("AND")
    emoteCount = 0
    if(emotes):
        emoteArray = getEmotes(message)
        parameters = parameters + emoteArray
        emoteCount = len(emoteArray)

        ##if this server is mentioned then only emotes on that server pass the sql test
        if(" server " in " " + message.content.lower() + " "):
            for serverEmoji in message.guild.emojis:
                emoteCount += 1
                parameters.append(serverEmoji.name.lower())

        if(emoteCount > 0):
            SQLString.append("LOWER(emoteName) IN (" + ("%s," * (emoteCount - 1)) + "%s)")


    ##for each channel mentioned filter it
    channelCount = 0
    if(channels):
        channelString = []
        for channel in message.channel_mentions:
            channelCount += 1
            channelString.append("channel_id = %s")
            parameters.append(channel.id)
        if(channelString):
            SQLString.append(" OR ".join(channelString))

    #for each user mentioned, filter it
    userTotal = 0
    userString = []
    if(users):
        userArray = getUsers(message, getUser = False)
        parameters = parameters + userArray
        userTotal = userTotal + len(userArray)

    ##roles are basically a list of users as roles are not stored with messages
    if(roles):
        ##roles are highlighted using () to avoid people tagging them
        listOfRoles = getRoles(message, getRole = True)
        ##for each member in the role allow that users messages to be used in the query
        for member in message.guild.members:
            for memberRole in member.roles:
                if(memberRole in listOfRoles):
                    userTotal = userTotal + 1
                    # userString.append("user_id = %s")
                    parameters.append(member.id)

    if(userTotal > 0):
        SQLString.append("user_id IN (" + ("%s," * (userTotal - 1)) + "%s)")

    ##allows users to serach by date
    dateString = []
    if(dates):
        ##searches for all dates in the format dd/mm/yyyy
        dateMatches = regex.findall(r"((\d+)\/(\d+)\/(\d\d\d\d))", message.content)

        if(len(dateMatches) > 2):
            message.channel.send("Too many dates provided, please give 1 or 2")
            return

        sign = ">="

        for date in dateMatches:
            #try used to make sure the dates are valid
            try:
                ##take in the date that is in the format %d/%m/%Y
                searchDate = datetime.datetime.strptime(date[0], "%d/%m/%Y")
                #date time is done between the two values
                dateString.append("datetime " + sign + " %s")
                ##date added as a parameter, converting date to string gives us the right format
                parameters.append(str(searchDate))
                #make sure the sign changes for the 2nd round
                sign = "<"
            except ValueError:
                await message.channel.send("Date: " + date[0] + " is not valid, please use the format DD/MM/YYYY")
                return
                

    ##date needs AND instead of OR so is done seperately
    if(len(dateString) == 2):
        SQLString.append("(" + " AND ".join(dateString) + ")")
    elif(len(dateString) == 1):
        SQLString.append("(" + dateString[0] + ")")

    ##join all bigger arrays by AND      (user_id = ? OR user_id = ?) AND (emoteName = ? AND emoteName = ?)
    if(len(SQLString) > 0):
        stringJoin = " AND " +" AND ".join(SQLString)
        return {"String": stringJoin, 
        "Parameters": parameters, 
        #if there are more than 1 for a section then it should be shown as it won't just show one thing
        "valuesFound": {"emote": emoteCount > 0, "channel": channelCount > 0, "user": userTotal > 0},
        "count": {"emote": emoteCount, "channel": channelCount, "user": userTotal}}
    else:
        return {"String": "", "Parameters": [], "valuesFound": {"emote": False, "channel": False, "user": False, "role": False},
        "count": {"emote": 0, "channel": 0, "user": 0}}

def getRoles(message, getRole = True, allowFullMessage = False):
    roleArray = []
    roleMatches = regex.findall(r"\(([^)]*)\)", message.content)
    for role in message.guild.roles:
        for roleInput in roleMatches:
            ##finds the role mentioned in the brackets ( )
            if(role.name.lower().replace(" ", "") == roleInput.lower().replace(" ", "")):
                roleArray.append(role)

        if(allowFullMessage):
            if(role.name.lower().replace(" ", "") == message.content.lower().replace(" ", "")):
                roleArray.append(role)

    for role in message.role_mentions:
        roleArray.append(role)

    if(not getRole):
        roleIdArray = []
        for role in roleArray:
            roleIdArray.append(role.id)
        return roleIdArray
    return roleArray

def getUsers(message, getUser = True, allowFullMessage = False):
    userArray = []
    userMatches = regex.findall(r"\[([^\]]*)\]", message.content)
    for userMatch in userMatches:
        for member in message.guild.members:
            ##finds the user mentioned in the brackets [ ]
            reducedInput = userMatch.lower().replace(" ", "")
            if member.name.lower().replace(" ", "") == reducedInput or (member.nick and member.nick.lower().replace(" ", "") == reducedInput):
                userArray.append(member)

            if(allowFullMessage):
                if(member.name.lower().replace(" ", "") == message.content.lower().replace(" ", "") or (member.nick and member.nick.lower().replace(" ", "") == message.content.lower().replace(" ", ""))):
                    roleArray.append(role)

    for user in message.mentions:
        userArray.append(user)

    if(not getUser):
        userIdArray = []
        for user in userArray:
            userIdArray.append(user.id)
        return userIdArray
    return userArray

def getEmotes(message):
    emoteArray = []
    emoteMatches = regex.findall(r"(<:(\w+):\d+>)(?!.*\1)", message.content)
    for emote in emoteMatches:
        emoteArray.append(emote[1].lower())

    emoteMatches = regex.findall(r"(:(\w+):)(?!.*\1)", message.content)
    for emote in emoteMatches:
        if(not emote[1].lower() in emoteArray):
            emoteArray.append(emote[1].lower())

    ##searchs for all unicode emotes
    for emote in emoji.UNICODE_EMOJI:
        if(emote in message.content):
            emoteArray.append(emoji.demojize(emote).replace(":", ""))

    return emoteArray


##works out the limit provided by the user, just a normal number, and the extra info which is any bolded number
def workOutLimit(message, value = False):
    limit = regex.findall(r"\s+(\d+)\s+", " " + message.content + " ")
    if(value):
        if(limit):
            return int(limit[0])
        else:
            return 5
    moreInfo = regex.findall(r"\*\*(\d+)\*\*", message.content)
    ##LIMIT 2,1 means from the 2nd row and for 1 rows, so it only gets 1 value at the specified row
    if(moreInfo):
        return " LIMIT " + str((int(moreInfo[0]) - 1)) + ",1"
    elif(limit):
        return " LIMIT " + limit[0]
    return " LIMIT 5"

##Works out if a % symbol is in the string
def containsPercentage(message):
    limit = regex.findall(r"\s+%\s+", " " + message.content + " ")
    if limit:
        return True
    return False

##removes the command from a message
def removeCommand(string):
    return regex.sub(r"(\$|\?)\w+", "", string).strip()

##removes the command from a message
def removeRoles(string):
    string = regex.sub(r"<@&\d+>", "", string).strip()
    string = regex.sub(r"\(([^)]*)\)", "", " " + string + " ").strip()
    return string

def replaceIDsWithTags(message, string):
    #Replace Roles with text
    for role in regex.findall(r"(<@&(\d+)>)",string):
        if message.guild.get_role(int(role[1])):
            string = string.replace(role[0], message.guild.get_role(int(role[1])).name)
    
    for channel in regex.findall(r"(<#(\d+)>)",string):
        if message.guild.get_channel(int(channel[1])):
            string = string.replace(channel[0], message.guild.get_channel(int(channel[1])).mention)

    for user in regex.findall(r"(<@(\d+)>)",string):
        if message.guild.get_member(int(user[1])):
            string = string.replace(user[0], message.guild.get_member(int(user[1])).display_name)
        else:
            string = string.replace(user[0], "")

    for emote in regex.findall(r"(<a?:(\w+):\d*>)",string):
        print(emote[0],emote[1])
        string = string.replace(emote[0], " " + emote[1] + " ")
    
    string = " " + string + " "

    for emote in message.guild.emojis:
        if emote.name.lower() in string.lower():
            string = string.replace(" " + emote.name + " " , str(emote))
            string = string.replace(" " + emote.name.lower() + " " , str(emote))


    string = emoji.emojize(string, use_aliases=True).strip()

    return string

def countMessageLengthIgnoringTags(string):
    string = regex.sub(r"<a?:(\w+):\d+>", "aaaaa",string)
    string = regex.sub(r"<@&\d+>", "aaaaa",string)
    string = regex.sub(r"<#\d+>", "aaaaa",string)
    string = regex.sub(r"<@\d+>", "aaaaa",string)
    string = regex.sub(r"<a?:(\w+):\d+>", "aaaaa",string)
    return len(string)

##removes the command from a message
def removeUsers(string):
    string = regex.sub(r"<@\d+>", "", string).strip()
    string = regex.sub(r"\[([^]]*)\)", "", " " + string + " ").strip()
    return string

##removes the command from a message
def removeEmotes(string):
    string = regex.sub(r"<a?:(\w+):\d+>", "", string).strip()
    string = regex.sub(r":(\w+):", "", string).strip()
    
    ##searchs for all unicode emotes
    for emote in emoji.UNICODE_EMOJI:
            string = string.replace(emote, "")
    return string

##removes emote id from a message giving just the emote string
def removeIDfromEmote(string):
    for emote in regex.findall(r"(<a?:(\w+):\d+>)", string):
        string = string.replace(emote[0], emote[1])
    return string

def combineEmotes(rows):
    x = 0
    while(x < len(rows)):
        for y in range(0, x):
            if(rows[x]['Emote'].lower() == rows[y]['Emote'].lower()):
                rows[y]['Total'] += rows[x]['Total']
                del rows[x]
                x = x - 1
                break
        x += 1
    rows = sorted(rows, key=lambda k: k['Total'], reverse=True) 
    return rows

##replaces plain text emotes with actual emotes so they show up on the server
def replaceEmoteWithEmoji(message, string):
    for emoji in message.guild.emojis:
        string = string.replace(emoji.name, str(emoji))
        string = regex.sub("<a?:" + emoji.name + ":\\d*?>", str(emoji), string)
    return string
    

def getTimeDefined(string, currentTime):
    timeTill = currentTime
    dateString = []
    #add time to the current date based on peoples input on the strings
    for second in regex.findall(r"(\d+)\s+second", string.lower()):
        dateString.insert(0,addPlural(second,"second"))
        timeTill = timeTill + datetime.timedelta(seconds=int(second))
        string = regex.sub(r"(\d+)\s+second(s)?", "", string)

    for minute in regex.findall(r"(\d+)\s+minute", string.lower()):
        dateString.insert(0,addPlural(minute,"minute"))
        timeTill = timeTill + datetime.timedelta(minutes=int(minute))
        string = regex.sub(r"(\d+)\s+minute(s)?", "", string)

    for hour in regex.findall(r"(\d+)\s+hour", string.lower()):
        dateString.insert(0,addPlural(hour,"hour"))
        timeTill = timeTill + datetime.timedelta(hours=int(hour))
        string = regex.sub(r"(\d+)\s+hour(s)?", "", string)

    for days in regex.findall(r"(\d+)\s+day", string.lower()):
        dateString.insert(0,addPlural(days,"day"))
        timeTill = timeTill + datetime.timedelta(days=int(days))
        string = regex.sub(r"(\d+)\s+day(s)?", "", string)

    for week in regex.findall(r"(\d+)\s+week", string.lower()):
        dateString.insert(0,addPlural(week,"week"))
        timeTill = timeTill + datetime.timedelta(weeks=int(week))
        string = regex.sub(r"(\d+)\s+week(s)?", "", string)

    for month in regex.findall(r"(\d+)\s+month", string.lower()):
        dateString.insert(0,addPlural(month,"month"))
        timeTill = timeTill + datetime.timedelta(months=int(month))
        string = regex.sub(r"(\d+)\s+month(s)?", "", string)

    dateStringReturn = ""
    if(len(dateString) > 2):
        dateStringReturn = ", ".join(dateString[0:-1])
        dateStringReturn += " and " + dateString[-1]
    else:
        dateStringReturn = " and ".join(dateString)
        
    return {"date": timeTill, "dateString": dateStringReturn, "stringRemoved": string}

def addPlural(number, string):
    if(int(number) == 1):
        return str(number) + " " + string
    elif(int(number) > 1):
        return str(number) + " " + string + "s"

def replaceRolesWithText(message,string):
    for roleRegex in regex.findall(r"<@&(.*?)>", string):
        for role in message.guild.roles:
            if str(role.id) == str(roleRegex):
                string = string.replace("<@&" + roleRegex + ">", "'" + role.name + "'")
    string = string.replace("@here", "@" + " here")
    string = string.replace("@everyone", "@" + " everyone")
    return string

def replaceUserWithText(message,string):
    print("Executed")
    for userRegex in regex.findall(r"<@(.*?)>", string):
        print("User Found", userRegex)
        for user in message.guild.members:
            if str(user.id) == str(userRegex):
                print("FOUND")
                string = string.replace("<@" + userRegex + ">", "'" + user.display_name + "'")
    return string

def getTimes(string):
    today = datetime.date.today()

    for yesterday in regex.findall(r"yesterday", string.lower()):
        return {'date1': today + datetime.timedelta(days=-1), 'date2': today, "string": regex.sub(r"yesterday", "", string)}

    for week in regex.findall(r"this\s+week", string.lower()):
        today = today + datetime.timedelta(days=-today.weekday())
        return {'date1': today, "string": regex.sub(r"this\s+week", "", string)}

    for day in regex.findall(r"last\s+(\d+)*\s*day(s)*", string.lower()):
        startDate = today + datetime.timedelta(days = int(day[0]) * -1)
        return {'date1': startDate, "date2": today, "string": regex.sub(r"last\s+(\d+)*\s*day(s)*", "", string)}
    
    for week in regex.findall(r"last\s+(\d+)*\s*week(s)*", string.lower()):
        EndWeek = today + datetime.timedelta(days=-today.weekday())
        StartWeek = None
        if(week[0].isdigit()):
            StartWeek = EndWeek + datetime.timedelta(weeks = int(week[0]) * -1)
        else:
            StartWeek = EndWeek + datetime.timedelta(weeks = int(-1))
        return {'date1': StartWeek, "date2": EndWeek, "string": regex.sub(r"last\s+(\d)*\s*week(s)*", "", string)}

    for month in regex.findall(r"this\s+month", string.lower()):
        today = today.replace(day=1)
        return {'date1': today, "string": regex.sub(r"this\s+month", "", string)}

    for month in regex.findall(r"last\s+(\d+)*\s*month(s)*", string.lower()):
        endMonth = today.replace(day=1)
        startMonth = endMonth
        monthsToRemove = None
        if(month[0].isdigit()):
            monthsToRemove = today.month - int(month[0])
        else:
            monthsToRemove = today.month - 1
        while(monthsToRemove < 1):
            startMonth = startMonth.replace(year=(startMonth.year - 1))
            monthsToRemove += 12
        startMonth = startMonth.replace(month = monthsToRemove) 

        return {'date1': startMonth, "date2": endMonth, "string": regex.sub(r"last\s+(\d)*\s*week(s)*", "", string)}

    months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    monthShort = ["Jan","Feb","March","April","May","June","July","August","Sept","Oct","Nov","Dec"]
    for x in range(0,2):
        if(x == 1):
            months = monthShort

        for x in range(0,12):
            index = string.find(months[x].lower())
            if(index >= 0):
                string = string[:index] + string[index + len(months[x]):]
                endDate = today.replace(day=1)
                endDate = endDate.replace(month = x + 1)
                
                if(endDate > datetime.date.today()):
                    endDate = endDate.replace(year=(endDate.year - 1))

                startDate = None
                if(x == 11):
                    startDate = endDate.replace(month = 1)
                    startDate = startDate.replace(year=(endDate.year + 1))
                else:
                    startDate = endDate.replace(month = endDate.month + 1)
                return {'date1': endDate, "date2": startDate, "string": string}
    return False







