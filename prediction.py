import regex
import runsql
import random
import discordBotFunctions

async def createPrediction(message):
    if(not await discordBotFunctions.checkUser(message)):
        return

    predictionData = message.content.split("\n")
    optionsStarted = False
    predictionInformation = ""
    options = []
    for data in predictionData:
        matches = regex.findall(r"(\d+)\)(.*)", data)
        if(matches):
            optionsStarted = True
            options.append(matches[0][1].strip())
        elif(not optionsStarted):
            predictionInformation += data

    if(not predictionInformation):
        await message.channel.send("You need to provide information for the prediction")
        return

    if(not options):
        await message.channel.send("You need to provide options for the prediction")
        return

    sql_id = runsql.insertAndGetID("INSERT INTO prediction (title, server_id) VALUES (%s,%s)", [predictionInformation.strip(), message.guild.id])
    for option in options:
        runsql.executeSQL("INSERT INTO prediction_options (prediction_id, title) VALUES (%s,%s)", [sql_id, option])
    await message.channel.send("A prediction has been created:")
    await printPrediction(message, sql_id, actual_id = True)
    
async def printPrediction(message, prediction_id, previous = False, actual_id = False):
    if(actual_id):
        withrows = "prediction_with_rows.id = %s"
    else:
        withrows = "prediction_with_rows.row_number = %s"

    if(previous):
        answer = "prediction.answer <> 0"
    else:
        answer = "prediction.answer = 0"

    sql = """
    SELECT * FROM (
        SELECT prediction.*, @rownum := @rownum + 1 AS row_number
        FROM prediction, (SELECT @rownum := 0) r
        WHERE server_Id = %s AND {answer}
        ORDER BY open DESC, id ASC) as prediction_with_rows
    WHERE {withrows}
    """.format(answer=answer, withrows=withrows)

    prediction = runsql.executeSingleSQL(sql, [message.guild.id, prediction_id])

    if(not prediction):
        await message.channel.send("That prediction number does not exist, to view all predictions type ?prediction")
        return
    
    predictionOptions = runsql.executeSQL("SELECT * FROM prediction_options WHERE prediction_id = %s", prediction['id'])
    
    predictionString = prediction['title'] + "\n"
    predictionString += "=" * len(prediction['title']) + "\n"

    number = 1
    for option in predictionOptions:
        winner = ""
        if(previous and option['id'] == prediction['answer']):
            winner = "**WINNER**"
        predictionString += str(number) + ") " + option['title'] + " " +winner +  "\n"
        users = runsql.executeSQL("SELECT user_id FROM user_prediction WHERE prediction_id = %s AND prediction_option_id = %s", [prediction['id'], option['id']])
        actual_users = discordBotFunctions.getUsersFromIds(message, users, names = True)
        if actual_users:
            predictionString += "--" + ",".join(actual_users) + "\n"
        number += 1
        
    if(not previous and prediction['open'] == 1):
        ##gets the value required to vote for the prediction
        predictionValue = str(int(prediction['row_number']))

        ##looks to see if there has been a tag set by the user
        if(prediction['tag']):
            predictionValue = prediction['tag']

        predictionString += "\nTo vote in this prediction type: ?predict " + predictionValue + " [option number or option name]"
    ##adds markdown to the prediction
    predictionString = "```Markdown\n" + predictionString + "```"
    await message.channel.send(predictionString)

async def currentPredictions(message):
    if(message.content == ""):
        rows1 = runsql.executeSQL("SELECT * FROM prediction WHERE server_id = %s AND open = 1 AND answer = 0", message.guild.id)
        predictionString = ""
        number = 1
        if(rows1):
            topString = "Current Open Predictions:"
            predictionString += topString + "\n"
            predictionString += "=" * len(topString) + "\n"
            for row in rows1:
                predictionString += str(number) + ") " + row['title'] + "\n"
                number += 1

        rows2 = runsql.executeSQL("SELECT * FROM prediction WHERE server_id = %s AND open = 0 AND answer = 0", message.guild.id)
        if(rows1 and rows2):
            predictionString += "\n"
        if(rows2):
            middleString = "Current Closed Predictions with no result yet:"
            predictionString += middleString + "\n"
            predictionString += "=" * len(middleString) + "\n"
            
            for row in rows2:
                predictionString += str(number) + ") " + row['title'] + "\n"
                number += 1
        
        predictionString += "\nTo view a prediction in more detail type ?prediction [prediction number]"
        predictionString = "```Markdown\n" + predictionString + "```"
        await message.channel.send(predictionString)
        return

    info = await getPrediction(message, True)
    if(info):
        await printPrediction(message, info['prediction']['id'], actual_id = True)
    else:
        await message.channel.send("Please define a number after using ?prediction or use ?prediction to view all")

async def oldPredictions(message):
    if(message.content == ""):
        
        predictionString = ""
        
        topString = "All previous Predictions:"
        predictionString += topString + "\n"
        predictionString += "=" * len(topString) + "\n"

        rows = runsql.executeSQL("SELECT * FROM prediction WHERE server_id = %s AND answer <> 0", message.guild.id)
        number = 1
        for row in rows:
            predictionString += str(number) + ") " + row['title'] + "\n"
            number += 1
        
        predictionString += "\nTo view a prediction in more detail type ?predictionold [prediction number]"
        predictionString = "```Markdown\n" + predictionString + "```"
        
        await message.channel.send(predictionString)
    elif(message.content.isdigit() ):
        await printPrediction(message, int(message.content), previous = True)
    else:
        await message.channel.send("Please define a number after using ?predictionold or use ?predictionold to view all")

async def votePrediction(message):
    info = await getPrediction(message)
    if(not info):
        return

    if(not runsql.executeSingleSQL("SELECT 1 FROM prediction WHERE id = %s AND open = 1", info['prediction']['id'])):
        await message.channel.send("This prediction is closed and you cannot vote for it")
        return

    row = runsql.executeSingleSQL("SELECT id FROM user_prediction WHERE prediction_id = %s AND user_id = %s", [info['prediction']['id'], message.author.id])
    if(row):
        runsql.executeSQL("UPDATE user_prediction SET prediction_option_id = %s WHERE id = %s", [info['option']['id'], row['id']])
    else:
        runsql.executeSQL("INSERT INTO user_prediction (prediction_id, user_id, prediction_option_id) VALUES (%s,%s,%s)", [info['prediction']['id'], message.author.id, info['option']['id']])

    await message.channel.send("You have predicted '" + info['option']['title'] + "' for '" + info['prediction']['title'] + "'")

async def stopPrediction(message):
    if(not await discordBotFunctions.checkUser(message)):
        return
    info = await getPrediction(message, True)
    if(not info):
        return

    runsql.executeSQL("UPDATE prediction SET open = 0 WHERE id = %s", [info['prediction']['id']])

    await message.channel.send("The prediction '" + info['prediction']['title'] + "'' has been closed to predictions")

async def openPrediction(message):
    if(not await discordBotFunctions.checkUser(message)):
        return
    info = await getPrediction(message, True)
    if(not info):
        await message.channel.send("To open a prediction please write ?OpenPrediction [prediction number]")
        return
    runsql.executeSQL("UPDATE prediction SET open = 1 WHERE id = %s", [info['prediction']['id']])

    await message.channel.send("The prediction '" + info['prediction']['title'] + "'' has been opened to predictions")

async def predictionWinner(message):
    if(not await discordBotFunctions.checkUser(message)):
        return
    info = await getPrediction(message)
    if(not info):
        await message.channel.send("To give an answer to a prediction prediction please write ?SetWinner [prediction number] [option number or option name]")
        return
    runsql.executeSQL("UPDATE prediction SET open = 0, answer = %s WHERE id = %s", [info['option']['id'], info['prediction']['id']])
    winners = runsql.executeSQL("SELECT user_id FROM user_prediction WHERE prediction_id = %s and prediction_option_id = %s", [info['prediction']['id'], info['option']['id']])
    if(winners):  
        winner = discordBotFunctions.getUsersFromIds(message, winners, names = True)
        await message.channel.send("Prediction has been completed. Winners are: " + ",".join(winner))
    else:
        await message.channel.send("Prediction has been completed. There were no winners")

async def setPredictionTag(message):
    info = await getPrediction(message, tag = True)
    if(not info):
        return

    runsql.executeSQL("UPDATE prediction SET tag = %s WHERE id = %s", [info['option'], info['prediction']['id']])
    await message.channel.send("The prediction '" + info['prediction']['title'] + "' can now be voted for using ?predict " + info['option'] + " [prediction name or number]")


async def getPrediction(message, justPrediction = False, previous = False, tag = False):
    
    sqlEnd = " AND answer = 0"
    if(previous):
        sqlEnd = " AND answer <> 0"
    sqlEnd += " ORDER BY open DESC, id ASC"
    split = message.content.strip().split(" ",1)

    serverPredictions = runsql.executeSQL("SELECT * FROM prediction WHERE server_id = %s" + sqlEnd, message.guild.id)
    prediction = None
    if(split[0].isdigit()):
        if (int(split[0]) > len(serverPredictions) or len(split[0]) < 1):
            await message.channel.send("This string is not valid type '?waitwhat prediction' for more information")
            return False
        else:
            prediction = serverPredictions[int(split[0]) - 1]
    else:
        for predict in serverPredictions:
            if(predict['tag'].lower() == split[0].lower()):
                prediction = predict
                break

    if(not prediction):
        await message.channel.send("This string is not valid type '?waitwhat prediction' for more information")
        return False
    
    if(justPrediction):
        return {"prediction": prediction}

    if(tag):
        if(split[1].isdigit() or len(split[1]) > 15 or " " in split[1]):
            await message.channel.send("The tag must not be a number or longer than 15 characters")
            option = split[1]
        return {"prediction": prediction, "option": split[1]}

    predictionOptions = runsql.executeSQL("SELECT * FROM prediction_options WHERE prediction_id = %s", prediction['id'])
    option = None
    if(split[1].isdigit()):
        if int(split[1]) > len(predictionOptions) or len(split[1]) < 1:
            await message.channel.send("This string is not valid type '?waitwhat prediction' for more information")
            return False
        else:
            option = predictionOptions[int(split[1]) - 1]
    else:
        for predictOption in predictionOptions:
            if(predictOption['title'].replace(" ","").lower() == split[1].replace(" ","").lower()):
                option = predictOption
                break
    if(not option):
        await message.channel.send("This string is not valid type '?waitwhat prediction' for more information")
        return False

    return {"prediction": prediction, "option": option}

