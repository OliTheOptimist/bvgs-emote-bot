import regex
import emoji

##prints a table style message in chat
async def printTable(message, tableRows, edit=False):

    if(not tableRows):
        await message.channel.send("No Data Found :(")
        return

    ##sorts table into appropriate format
    tableRows = await swapIdForValue(message, tableRows)
    ##create an array of the length of each column so all text lines up appropriately
    tableLengths = [0] * len(tableRows[0])

    ##gets the max length of each collumn based on the values from the table
    for x in range(0, len(tableRows)):
        for y in range(0, len(tableRows[x])):
            lengthOfString = len(str(tableRows[x][y]))
            #unicode emotes take up two spaces 
            for c in str(tableRows[x][y]):
                if c in emoji.UNICODE_EMOJI:
                    lengthOfString = lengthOfString + 1
            if(lengthOfString > tableLengths[y]):
                tableLengths[y] = len(tableRows[x][y])

    ##string that will be posted at the end
    tableString = [""]
    current = 0

    ##variable to see if teh table split has been added
    splitAdded = False
    
    for x in range(0, len(tableRows)):
        ##Add in title spacing whhich is the same as max length
        if(x == 1 and not splitAdded):
            for y in range(0, len(tableLengths)):
                tableString[current] += ('=' * (tableLengths[y] + 3))
                tableString[current] += ('-' * 3)
            tableString[current] += "\n"
            splitAdded = True

        ##prints the text for that column and then adds spacing so it lines up
        for y in range(0, len(tableRows[x])):
            tableString[current] += tableRows[x][y]
            tableString[current] += " " * (tableLengths[y] - len(tableRows[x][y]) + 6)
        tableString[current] += "\n"
        if len(tableString[current]) > 1900:
            current += 1
            tableString.append("")
    if(len(tableString) >= 4):
        await message.channel.send("This query has too many characters")
        return

    for tableMessage in tableString:           
        if(edit):
            await message.edit(content = "```Markdown\r\n" + tableMessage + "```")
        else:
            await message.channel.send("```Markdown\r\n" + tableMessage + "```")

##prints it as a detailed list instead of a table format
async def printList(message, tableRows, edit=False):

    if(not tableRows):
        await message.channel.send("No Data Found :(")
        return

    ##gets it in the appropriate information
    tableRows = await swapIdForValue(message, tableRows)

    tableString = ""
    
    for x in range(1, len(tableRows)):

        ##Add in spacing between each list
        if(x == 1):
            tableString += ('=' * 20) + "\n"
        ##prints the title and then the information
        for y in range(0, len(tableRows[0])):
            tableString += tableRows[0][y] + ": " + str(tableRows[x][y]) + "\n"

        #tableString += ('=' * 20) + "\n"

    tableString = "```Markdown\r\n" + tableString + "```"

    if(edit):
        await message.edit(content = tableString)
    else:
        await message.channel.send(tableString)

##stores the information in table format, first row is titles and the rest are info
async def swapIdForValue(message, tableRows):
    tableInfo = []
    titles = []

    ##add the titles based on the keys given
    for key in tableRows[0]:
        titles.append(key)
    ##adds the tile row to the main table
    tableInfo.append(titles)

    for row in tableRows:
        rowInfo = []
        ##converts information from the database that have id's to information that people can understand
        ##i.e. user_id to user.display_name
        for key in tableRows[0]:
            if(key == "User" or key == "Ended By User" or key == "Started By User"):
                user = message.guild.get_member(row[key])
                if(user):
                    ##for the user removes all unusual commands so they don't mess up spacing in the table
                    rowInfo.append(regex.sub(r'[^\x00-\x7f]',r'',user.display_name))
                else:
                    rowInfo.append("Unknown User")
            elif(key == "Channel"):
                channel = message.guild.get_channel(row[key])
                if(channel):
                    rowInfo.append(channel.name)
                else:
                    rowInfo.append("Unknown Channel")
            elif(key == "Started at" or key == "Ended at"):
                rowInfo.append(row[key].strftime("%d/%m/%Y %H:%M:%S"))
            else:
                rowInfo.append(str(row[key]))
        tableInfo.append(rowInfo)

    return tableInfo

