import random
import runsql
import datetime

async def getRandomUser(message):
    ##gets all users that have posted in the last 2 days
    timeTill = datetime.datetime.now()
    timeTill = timeTill + datetime.timedelta(days=-2)
    rows = runsql.executeSingleSQL("""
        SELECT user_id FROM 
            (SELECT user_id, count(message) as TotalMessages 
            FROM message
            WHERE server_id = %s
            AND datetime >= CURDATE() - INTERVAL 2 WEEK
            GROUP BY user_id) AS potential_users
            WHERE TotalMessages > 60 ORDER BY Rand() LIMIT 1""", [message.guild.id])

    if(not rows):
        userName = "There are not enough users for this command to work"
        return
    userName = message.guild.get_member(rows['user_id']).display_name
    
        
    await message.channel.send(userName)
