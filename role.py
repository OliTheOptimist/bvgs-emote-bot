import operator
import runsql
import printStats
import parsemessage
import datetime
import math

async def roleStats(message):
    gameOnly = " rolegroups " in  " " + message.content.lower() + " "
    acceptRoles = parsemessage.getRoles(message, getRole = True, allowFullMessage = True)
    acceptUsers = parsemessage.getUsers(message, getUser = True, allowFullMessage = True)

    gameids = []
    if(gameOnly):
        rows = runsql.executeSQL("SELECT role_id FROM role_tags INNER JOIN role_tagger_group ON role_tags.role_group_id = role_tagger_group.id WHERE server_id = %s", [message.guild.id])
        if(not rows):
            await message.channel.send("You haven't set up any role groups on this server")
        for row in rows:
            gameids.append(row['role_id'])
    roleTotal = []
    if len(acceptRoles) == 1:
        roleTotal.append({"Role Name": acceptRoles[0].name, "Total Users": len(acceptRoles[0].members)})
        names = []
        for user in acceptRoles[0].members:
            names.append(user.display_name)
        roleTotal[0]["Users with roles tagged"] = "" ", ".join(names)
        rows = runsql.executeSingleSQL("SELECT COUNT(id)/4 as Total FROM message WHERE message LIKE %s AND server_id = %s AND `datetime` >= CURDATE() - INTERVAL 4 WEEK", ["%<@&" + str(acceptRoles[0].id) + ">%", message.guild.id])
        if(rows['Total'] == 0):
            roleTotal[0]['Average tags per week'] = "This game has not been tagged"
        else:
            roleTotal[0]['Average tags per week'] = round(rows['Total'],2)

        await printStats.printList(message, roleTotal)
    else:        
        for role in message.guild.roles:
            print(role.name, gameOnly, role.id not in gameids)
            if gameOnly and role.id not in gameids:
                continue
            if role.is_default():
                continue
            if acceptUsers:
                skip = True
                for user in acceptUsers:
                    if role in user.roles:
                        skip = False
                        break
                if skip:
                    continue
            roleTotal.append({"Role Name": role.name, "Total Users": len(role.members)})

        if " asc" in message.content:
            roleTotal.sort(key=lambda x: x["Total Users"], reverse=False)
        else:
            roleTotal.sort(key=lambda x: x["Total Users"], reverse=True)

        limitValue = parsemessage.workOutLimit(message, True)

        if(limitValue < len(roleTotal)):
            roleTotal = roleTotal[0:limitValue]

        await printStats.printTable(message, roleTotal)




    # for user in acceptUsers:
    #     userRoles = userRoles + user.roles

    # for member in message.guild.members:
    #     for role in member.roles:
    #         print(role)
    #         # if(role.is_default()):
    #         #     continue
    #         if(userRoles and not role in userRoles):
    #             continue
    #         if(gameOnly and not str(role.id) in gameids):
    #             continue
    #         if(acceptRoles and not str(role.id) in acceptRoles):
    #             continue
    #         elif(acceptRoles and (not usersWithRole or not usersWithRole[-1] == member.display_name)):
    #             usersWithRole.append(member.display_name)

    #         index = -1
    #         for x in range(0, len(roleTotal)):
    #             if(roleTotal[x]['id'] == role.id):
    #                 index = x
    #                 break

    #         if(index >= 0):
    #             roleTotal[x]["Total Users"] = roleTotal[x]["Total Users"] + 1
    #         else:
    #             roleTotal.append({"id": role.id, "Role Name": role.name, "Total Users": 1})
    
    # for role in message.guild.roles:
    #     if role.id in gameids:
    #         breakLoop = False
    #         for roleItem in roleTotal:
    #             if(roleItem["id"] == role.id):
    #                 breakLoop = True
    #                 break
    #         if breakLoop == False:
    #             roleTotal.append({"id": role.id, "Role Name": role.name, "Total Users": 0})















