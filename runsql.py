import MySQLdb
import MySQLdb.cursors
import emoji
#139.59.191.97
##executes an sql command nice and easily using a string and optional parameters
def executeSQL(sqlString , parameters = []):
    #print(sqlString)
    #print(parameters)
    try:
        db = MySQLdb.connect(host="localhost", user="oli", passwd="MingLee123", db = "emote_bot_data_restore", charset = "utf8mb4", use_unicode=True, cursorclass=MySQLdb.cursors.DictCursor)
        cur = db.cursor()
        if not isinstance(parameters, list):
            parameters = [parameters]
        cur.execute(sqlString, parameters)
        rows = cur.fetchall()
        db.commit()
        db.close()
    except Exception as e:
        print(e)
        return
    return list(rows)

def insertAndGetID(sqlString , parameters = []):
    #print(sqlString)
    #print(parameters)
    try:
        db = MySQLdb.connect(host="localhost", user="oli", passwd="MingLee123", db = "emote_bot_data_restore", charset = "utf8mb4", use_unicode=True, cursorclass=MySQLdb.cursors.DictCursor)
        cur = db.cursor()
        if not isinstance(parameters, list):
            parameters = [parameters]
        cur.execute(sqlString, parameters)
        lastID = cur.lastrowid
        db.commit()
        db.close()
    except Exception as e:
        print(e)
        return
    return lastID

##executes an sql command nice and easily using a string and optional parameters
def executeSingleSQL(sqlString , parameters = []):
    #print(sqlString)
    #print(parameters)
    try:
        db = MySQLdb.connect(host="localhost", user="oli", passwd="MingLee123", db = "emote_bot_data_restore", charset = "utf8mb4", use_unicode=True, cursorclass=MySQLdb.cursors.DictCursor)
        cur = db.cursor()
        if not isinstance(parameters, list):
            parameters = [parameters]
        cur.execute(sqlString, parameters)
        row = cur.fetchone()
        db.commit()
        db.close()
    except Exception as e:
        print(e)
        return
    return row



