import parsemessage
import datetime
import regex
import runsql
import synonyms
import math

async def giveMessageCipher(client, message, commands):
	listOfWordsToRemove = ["showing", "and", "limit"]
	
	for word in listOfWordsToRemove:
		message.content = message.content.replace(word, "")

	newString = ""
	usersFound = []
	rolesFound = []
	emoteFound = []
	datesFound = False
	findCommand = False
	predictions = [False, False]
	spelling = False
	for x in range(0,2):

		if(not spelling):
			usersFound = parsemessage.getUsers(message)
			message.content = parsemessage.removeUsers(message.content)
			foundUsers = await searchString(client, message, message.content, message.guild.members, "display_name", allowSpellings = False)
			message.content = foundUsers['string']
			usersFound = usersFound + foundUsers['results']
		else:
			foundUsers = await searchString(client, message, message.content, message.guild.members, "display_name", allowSpellings = True)
			message.content = foundUsers['string']
			usersFound = usersFound + foundUsers['results']

		if(not spelling):
			rolesFound = parsemessage.getRoles(message)
			message.content = parsemessage.removeRoles(message.content)
		else:
			foundRoles = await searchString(client, message, message.content, message.guild.roles, "name", allowSpellings = True)
			message.content = foundRoles['string']
			rolesFound = rolesFound + foundRoles['results']

		if(not spelling):
			emoteFound = parsemessage.getEmotes(message)
			message.content = parsemessage.removeEmotes(message.content)
		else:
			emotes = runsql.executeSQL("SELECT emoteName FROM emote WHERE server_id = %s GROUP BY emoteName", message.guild.id)

			foundEmote = await searchString(client, message, message.content, emotes, "emoteName", allowSpellings = True)
			message.content = foundEmote['string']
			emoteFound = emoteFound + [o['emoteName'] for o in foundEmote['results']]

		if(not spelling):
			datesFound = parsemessage.getTimes(message.content)

			if(datesFound):
				message.content = datesFound['string']
				if('date1' in datesFound):
					newString += " " + datesFound['date1'].strftime("%d/%m/%Y") + " "
				if('date2' in datesFound):
					newString += " " + datesFound['date2'].strftime("%d/%m/%Y") + " "

		if(not findCommand and x == 0):
			findCommand = searchTag(message.content, commands, "tags")
		if(not findCommand and x == 1):
			findCommand = searchTag(message.content, commands, "tags", allowSpellings = True)

		if(findCommand and x == 0):
			message.content = findCommand[0]["string"]

		if(findCommand):
			commandDetails = findCommand[0]

			if("predict" in commandDetails["commandName"]):
				if(predictions[0] == False):
					predictsql = runsql.executeSQL("SELECT * FROM prediction WHERE server_id = %s", message.guild.id)
					predictResults = await searchString(client, message, message.content, predictsql, "tag" , allowSpellings = not spelling)

					if(predictResults):
						predictions[0] = predictResults[0]
						newString = " " + predictResults[0]['tag']
						message.content = predictResults["string"]

				if(predictions[0] and not predictions[1]):
					optionssql = runsql.executeSQL("SELECT * FROM prediction_options WHERE prediction_id = %s", predictions[0]['id'])
					optionResults = await searchString(client, message, message.content, optionssql, "title", allowSpellings = not spelling)
					if(optionResults['results']):
						predictions[1] = True
						newString = " " + optionResults['results'][0]["title"]
						message.content = optionResults['string']
						break
			elif("emotestats" in commandDetails["commandName"] or "messagestats" in commandDetails["commandName"]):
				defineSearch = await searchString(client, message, message.content, ["user", "emote", "channel", "server"], allowPlural = True, allowSpellings = not spelling)
				message.content = defineSearch['string']
				for result in defineSearch['results']:
					newString += " " + result
			elif("rolestats" in commandDetails["commandName"]):
				defineSearch = await searchString(client, message, message.content, ["game"], allowPlural = True, allowSpellings = not spelling)
				message.content = defineSearch['string']
				for result in defineSearch['results']:
					newString += " " + result

			if(spelling):
				limitSearch = regex.findall(r"(\d+)", message.content)

				if(len(limitSearch) == 1):
					newString += " " + limitSearch[0]
					message.content = message.content.replace(limitSearch[0], "")

		spelling = True

	message.mentions = usersFound
	message.role_mentions = rolesFound

	message.content = newString + outputEmotes(emoteFound)
	if(not findCommand):
		#await client.send_message(message.channel, "Sorry I did not recognise this command")
		return
	await findCommand[0]['command'](client, message)

def parStr(string):
	return " " + regex.sub(r"[^a-zA-Z0-9\s+]", "", string.lower()).strip() + " "

def outputUsers(list):
	string = ""
	for user in list:
		string += user.mention + " "
	return string

def outputRoles(list):
	string = ""
	for role in list:
		string += role.mention + " "
	return string

def outputEmotes(list):
	string = ""
	for emote in list:
		string += " :" + emote + ": "
	return string


async def searchString(client, message, string, array, option = False, allowPlural = False, allowSpellings = True):
	objectCollection = []
	corrections = []
	string = parStr(string)
	for item in array:
		stringItem = None
		if(option):
			if(isinstance(item, dict)):
				stringItem = parStr(item[option])
			else:
				stringItem = parStr(getattr(item, option))
		else:
			stringItem = item

		if(stringItem.strip() == ""):
			continue
		stringSearch = lookForSpellingMistakes(string, stringItem, spellings = allowSpellings)

		if(not stringSearch and allowPlural):
			stringItem = stringItem + "s"
			stringSearch = lookForSpellingMistakes(string, stringItem, spellings = allowSpellings)
		if(stringSearch):
			string = stringSearch['string']
			if("corrections" in stringSearch and stringSearch['corrections']):
				await client.send_message(message.channel, "I am assuming that \"" + stringSearch['corrections'] + "\" is meant to be \"" + stringItem.strip() + "\"")
			objectCollection.append(item)

	return {"string": string, "results": objectCollection, "corrections": []}

def searchTag(string, array, option, allowSpellings = True):
	objectCollection = []
	string = parStr(string)
	##for each command get the tags that are in it
	for commandName, value in array.items():
		tagsToSearch = value[option]
		if(isinstance(value[option][0], str)):
			tagsToSearch = [value[option]]
		
		total = 0
		tempString = string

		##go through all the versions of tags avaliable
		for tagList in tagsToSearch:
			total = 0
			currentTotal = 0
			##for all the individual tags in each set of tags
			for allTag in tagList:
				##set the default to only search for 1
				tagsToLookFor = [allTag]
				##looks to see if there is a list where multiple names are allowed
				for synonymList in synonyms.getSynonyms():
					if allTag in synonymList:
						##can search for all of the avaliable synonyms instead of just 1 word
						tagsToLookFor = synonymList
						break 
				##search through all the new tags
				for tag in tagsToLookFor:
					tag = parStr(tag)
					if tag in string:
						tempString = tempString.replace(tag.strip(), "", 1)
						currentTotal += 1
						break
			##keep the max total achieved
			if(currentTotal > total):
				total = currentTotal

		if(total > 0):
			value['percentage'] = float(total)/float(len(value[option]))
			value['string'] = tempString
			value['total'] = total
			value['commandName'] = commandName
			objectCollection.append(value)

	objectCollection = sorted(objectCollection, key=lambda k: (k['percentage'],k['total']), reverse=True)

	return objectCollection

def lookForSpellingMistakes(string, word, spellings = True):
	distanceCheck = 1
	matchInfo = []
	if(not spellings or len(word) < 5):
		if(word in string):
			return {"string": string.replace(word, " ")}
		else:
			return False

	string = (" " * distanceCheck) + string + (" " * distanceCheck)
	for x in range(distanceCheck, len(string) - distanceCheck):
		matchInfo.append({"completeness": 0, "positioning": 0, "startRange": math.inf, "endRange": 0})
		#print(word)
		for y in range (0, len(word)):
			minLook = x - distanceCheck + y
			maxLook = x + distanceCheck + y + 1
			lookIn = string[minLook:maxLook]
			#print(lookIn)
			#print("looking for " + word[y] + " in: " + lookIn)
			#print(word[y], lookIn)
			if(word[y] in lookIn):
				matchInfo[-1]['completeness'] += 1/len(word)
				matchFindAll = findAllChar(lookIn, word[y], distanceCheck)
				
				#print(matchFindAll)
				matchInfo[-1]['positioning'] += matchFindAll["position"]/len(word)
				if(word[y] != " "):
					if(matchFindAll["index"] < matchInfo[-1]['startRange']):
						print("replacing")
						#print("setting startRange to be " + str(x))
						matchInfo[-1]['startRange'] = x + matchFindAll["index"]
					if(matchFindAll["index"] != -1 and matchFindAll["index"] < matchInfo[-1]['startRange']):
						matchInfo[-1]['endRange'] = x + y + matchFindAll["index"]
					#print("setting endRange to be " + str(x+ y))
			#print(matchInfo[-1])

	finalList = []
	for match in matchInfo:
		if(match['completeness'] > 0.8 and match['positioning'] > 0.8):
			match['finalResult'] = round((match['completeness'] + match['positioning'])/2, 2)
			finalList.append(match)

	if(not finalList):
		return False
	else:
		maxResult = max(finalList, key=lambda x:x['finalResult'])
		returnDictionary = {
			"corrections": [],
			"list": maxResult,
		}
		startRemove = findClosestSpace(string, maxResult['startRange'])
		endRemove = findClosestSpace(string, maxResult['endRange'], False)
		
		if(word.strip() in string):
			returnDictionary["string"] = string.replace(word.strip(), "", 1)
		else:
			returnDictionary["string"] = string[:maxResult['startRange'] - 1] + " " + string[maxResult['endRange']:]
			#returnDictionary["string"] = string[:startRemove] + string[endRemove:]
		
		if(maxResult['finalResult'] != 1.0):
			returnDictionary['corrections'] = string[startRemove:endRemove]
		return returnDictionary

def findAllChar(string, char, middlePoint):
	position = 0.0
	indexLoc = -1
	indexes = []

	for x in range(0, len(string)):
		if(string[x] == char):
			indexes.append(x)

	for index in indexes:
		newPos = 1 - (abs(index - middlePoint) * 0.3)
		if(newPos > position):
			position = newPos
			indexLoc = index

	return {"position": round(position, 2), "index": indexLoc}

def findClosestSpace(string, index, prioritiseLeft = True):
	if(string[index] == " "):
		return index
	for x in range(1, len(string)):
		leftSpaceFound = False
		rightSpaceFound = False

		if(index - x < 0 or string[index - x] == " "):
			leftSpaceFound = True
		if(index + x >= len(string) or string[index + x] == " "):
			rightSpaceFound = True
		if(prioritiseLeft and leftSpaceFound):
			return index - x
		elif(not prioritiseLeft and rightSpaceFound):
			return index + x
		elif(leftSpaceFound):
			return index - x
		elif(rightSpaceFound):
			return index + x

	return False

#print(lookForSpellingMistakes("imtudent", "student", spellings = True))
