import datetime
import discord
import regex
import runsql
import parsemessage
import discordBotFunctions

async def addSinBin(message):
    binUser = False
    if(not await discordBotFunctions.checkUser(message)):
        binUser = True
        return

    #create duplicate dates to see if they are the same later
    tempDate = datetime.datetime.now()
    getTimeDefined = parsemessage.getTimeDefined(message.content, tempDate)
    timeTill = getTimeDefined['date']

    sinBinRole = discordBotFunctions.getRoleByString(message, "Sin Bin")

    users = []
    if(binUser):
        users.append(message.author)
    else:
        users = parsemessage.getUsers(message)

    if(not sinBinRole):
        await message.channel.send("Sin Bin tag not found")
        return
    elif(not users):
        await message.channel.send("No users found to tag")
        return

    #default time is 1 hour
    if(timeTill == tempDate or binUser):
        timeTill = timeTill + datetime.timedelta(hours=1)
        getTimeDefined['dateString'] = "1 hour"
    
    ##for each user and each role add the roles for a certain time
    for user in users:
        if(sinBinRole in user.roles):

            await message.channel.send("You cannot add " + sinBinRole.name + " to " + user.display_name + " as they already have this role")
            continue
        await user.add_roles(sinBinRole)
        if(user == message.author):
            await message.channel.send(parsemessage.replaceEmoteWithEmoji(message, sinBinRole.name + " added to " + message.author.display_name + " for " + getTimeDefined['dateString'] + " Jebaited"))
        else:
            await message.channel.send(sinBinRole.name + " added to " + user.display_name + " for " + getTimeDefined['dateString'])
        ##add when the user's role should time out
        #this could be changed later to store how long someone was "sinbinned" for and could potentially be a stat
        sql = "INSERT INTO role_until (user_id, role_id, server_id, until, channel_id) VALUES (%s,%s,%s,%s,%s)"
        parameters = [user.id, sinBinRole.id, message.guild.id, timeTill, message.channel.id]
        runsql.executeSQL(sql, parameters)

async def addHeart(message):
    if(not await discordBotFunctions.checkUser(message)):
        return

    #create duplicate dates to see if they are the same later
    tempDate = datetime.datetime.now()
    getTimeDefined = parsemessage.getTimeDefined(message.content, tempDate)
    timeTill = getTimeDefined['date']

    heartRole = discordBotFunctions.getRoleByString(message, "<3")

    users = parsemessage.getUsers(message)

    if(not heartRole):
        await message.channel.send("Heart tag not found")
        return
    elif(not users):
        await message.channel.send("No users found to tag")
        return

    #default time is 1 hour
    if(timeTill == tempDate):
        timeTill = timeTill + datetime.timedelta(hours=1)
        getTimeDefined['dateString'] = "1 hour"
    
    ##for each user and each role add the roles for a certain time
    for user in users:
        await user.add_roles(heartRole)
        if(user == message.author):
            await message.channel.send(parsemessage.replaceEmoteWithEmoji(message, "One can simply not add <3 to themselves Jebaited"))
            return
        else:
            await message.channel.send(heartRole.name + " added to " + user.display_name + " for " + getTimeDefined['dateString'])
            ##add when the user's role should time out
            #this could be changed later to store how long someone was "sinbinned" for and could potentially be a stat
            sql = "INSERT INTO role_until (user_id, role_id, server_id, until, channel_id) VALUES (%s,%s,%s,%s,%s)"
            parameters = [user.id, heartRole.id, message.guild.id, timeTill, message.channel.id]
            runsql.executeSQL(sql, parameters)