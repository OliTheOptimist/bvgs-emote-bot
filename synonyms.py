def getSynonyms():
	synonymList = [
	["add"],
	["emotes", "emote", "emoji", "emojis", "emoticon", "emoticons"],
	["stat", "stats", "statistics"],
	["message", "text"],
	["old", "previous"],
	["prediction", "predictions", "predict"],
	["view", "show"],
	["chain", "chains"]
	]
	return synonymList