import runsql
import parsemessage
import discordBotFunctions
import datetime 

class userTagging:
    taggingChannels = []
    rolesRequired = []
    startingChar = None
    tagYourselfMessages = []

    #load in user tag locations to stop people posting in there
    #selects roles that the user must have to talk
    def userTaggingLoad(self, startingC):
        print(startingC)
        self.startingChar = startingC
        print(self.startingChar)
        rows = runsql.executeSQL("SELECT channel_id FROM user_tag_location")
        for row in rows:
            self.taggingChannels.append(row['channel_id'])
        rows = runsql.executeSQL("SELECT role_id, server_id FROM possible_user_roles")
        for row in rows:
            self.rolesRequired.append(row)

    async def setUpUserTaggingHere(self, message):
        if(not await discordBotFunctions.checkUser(message)):
            return

        ##deletes message that set up the system
        await message.delete()
        ##creates a tempory message to be edited later when roles are added
        m = await message.channel.send("Tempory Message, please add roles")

        ##adds the location of the user tagging to the database
        sql = "INSERT INTO user_tag_location (server_id, channel_id, message_id) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE channel_id = %s, message_id = %s"
        runsql.executeSQL(sql, [message.guild.id, message.channel.id, m.id, message.channel.id, m.id])
        ##adds the channel as one to delete messages if they are posted
        self.taggingChannels.append(message.channel.id)
        ##if roles already exist for this server then add them back in
        if(runsql.executeSQL("SELECT 1 FROM possible_user_roles WHERE server_id = %s", [message.guild.id])):
            await self.updateRoleMessageForServer(message, False)

    ##updates the reason for having a role
    async def setDefinition(self, message):

        if(not await discordBotFunctions.checkUser(message)):
            return

        roles = parsemessage.getRoles(message, getRole = True, allowFullMessage = False)

        ##error checking, role must be mentioned because if you just write the Role when does the role name end and the definition start
        if(not roles):
            await message.channel.send("Please write the definition in the form @Role Example definition, you must actually tag the role", delete_after = 5)
            return
            
        for role in roles:
            row = runsql.executeSQL("SELECT 1 FROM possible_user_roles WHERE role_id = %s", [role.id])
            if(not row):
                await message.channel.send("That role does not exist for tagging users", delete_after=5)
            runsql.executeSQL("UPDATE possible_user_roles SET definition = %s WHERE role_id = %s", [parsemessage.removeRoles(message.content), role.id])
            await message.channel.send("Definition updated for " + role.name, delete_after=5)
        await self.updateRoleMessageForServer( message, False)

    ##disables game tagging
    async def stopUserTaggingHere(self, message):
        if(not await discordBotFunctions.checkUser(message)):
            return

        row = runsql.executeSQL("SELECT channel_id, message_id FROM user_tag_location WHERE server_id = %s", [message.guild.id])
        if(not row):
            await message.channel.send("User tagging is not set up")
            return

        ##removes channel from the block list so people can post in there again
        self.taggingChannels.remove(row[0]['channel_id'])
        roleMessage = await message.guild.get_channel(row[0]['channel_id']).fetch_message(row[0]['message_id'])
        await roleMessage.delete()

        runsql.executeSQL("DELETE FROM user_tag_location WHERE server_id = %s", [message.guild.id])
        await message.channel.send("User tagging has been stopped", delete_after=5)
        await message.delete()


    ##adds a tag to user tagging
    async def addRoleMain(self, message, ignore):
        if(not await discordBotFunctions.checkUser(message)):
            return

        roles = parsemessage.getRoles(message, allowFullMessage = True)
        ##tags can be added by tagging as well
        if(roles):
            for role in roles:
                ##checks to see if the tag already is added
                row = runsql.executeSQL("SELECT 1 FROM possible_user_roles WHERE role_id = %s", [role.id])
                if(row):
                    await message.channel.send("Role is already defined", delete_after=5)
                    return

                ##adds the tag based on the id of the role mentioned
                runsql.executeSQL("INSERT INTO possible_user_roles (server_id, role_id, ignore_tag) VALUES (%s, %s, %s)", [message.guild.id, role.id, ignore])
                self.rolesRequired.append({'role_id': role.id, 'server_id': message.guild.id})
                await message.channel.send("Added " + role.name + " to list of required roles", delete_after=5)
            ##updates the list of tags that can be tagged as a new one has been added
            await self.updateRoleMessageForServer(message, False)
        else:
            ##creates a new role
            newRole = await message.guild.create_role(name = message.content, mentionable = True)
            ##stores it in the database
            runsql.executeSQL("INSERT INTO possible_user_roles (server_id, role_id, ignore_tag) VALUES (%s, %s, %s)", [message.guild.id, newRole.id, ignore])
            self.rolesRequired.append({'role_id': newRole.id, 'server_id': message.guild.id})
            await message.channel.send("Added " + message.content + " to list of required roles", delete_after=5)
            await message.delete(delay=5)
            ##updates the list of tags that can be tagged as a new one has been added
            await self.updateRoleMessageForServer(message, False)
        message.delete(delay=3)

    async def addNormalRole(self, message):
        await self.addRoleMain(message, 0)

    async def addIgnoreRole(self, message):
        await self.addRoleMain(message, 1)

    ##removes tags from game tagging
    async def removeRole(self, message):
        if(not await discordBotFunctions.checkUser( message)):
            return

        roles = parsemessage.getRoles(message, getRole = True, allowFullMessage = True)

        if(not roles):
            await message.channel.send("No tagged roles found to remove, please tag a role to remove it", delete_after=5)
            return

        ##delete all roles found in the message
        for role in roles:
            if(not runsql.executeSQL("SELECT 1 FROM possible_user_roles WHERE role_id = %s", [role.id])):
                await message.channel.send("That role has not been set up for tagging", delete_after=5)
                return
            roleName = role.name
            runsql.executeSQL("DELETE FROM possible_user_roles WHERE server_id = %s AND role_id = %s", [message.guild.id, role.id])
            self.rolesRequired = [roleR for roleR in self.rolesRequired if not str(role.id) == str(roleR['role_id'])]
            await message.channel.send(roleName + " removed from the taggable role", delete_after=5)
        await message.delete()
        ##updates the list of avaliable roles to tag
        await self.updateRoleMessageForServer(message, False)


    async def updateRoleMessageForServer(self, message, fromCommand = True):
        row = runsql.executeSingleSQL("SELECT * FROM user_tag_location WHERE server_id = %s",[message.guild.id])
        channel = message.guild.get_channel(row['channel_id'])
        rows = runsql.executeSQL("SELECT * FROM possible_user_roles WHERE server_id = %s AND ignore_tag = 0 ORDER BY id DESC", [message.guild.id])
        messageString = "Welcome to " + message.guild.name + "! To have full access to this discord server you need to type one of the following commands:\n\n"
        
        gameList = []
        description = []
        
        ##creates a list of all the tags avaliable
        for role in rows:
            foundRole = message.guild.get_role(role['role_id'])
            gameList.append(foundRole.name)
            if role['definition']:
                description.append(role['definition'])
            else:
                description.append("")

        ##sort the list in alphabetical order
        #gameList.sort()

        ##add im to the beginning of each game
        for game in gameList:
            messageString += "?im " + game + "\n"

        messageString += "\nChoose\n"

        for x in range(len(gameList)):
            messageString += "- " + gameList[x] + " " + description[x] + "\n"
        
        userTagMessage = await channel.fetch_message(row['message_id'])
        await userTagMessage.edit(content=messageString)

        ##edits the message posted when the setupgametagging was created
        if(fromCommand):
            await message.channel.send("User tagging message has been updated", delete_after=5)
            await message.delete()

    async def roleTaggingMain(self, message):
        rows = runsql.executeSQL("SELECT 1 FROM user_tag_location WHERE server_id = %s", [message.guild.id])
        if(not rows):
            await message.channel.send("User tagging is not enabled on this server",delete_after=5)
            return

        gameName = message.content
        foundRoles = parsemessage.getRoles(message, allowFullMessage = True)

        if(not foundRoles):
            await message.channel.send("That role does not exist", delete_after=5)
            return

        rows = runsql.executeSQL("SELECT role_id FROM possible_user_roles WHERE server_id = %s and role_id = %s and ignore_tag = 0", [message.guild.id, foundRoles[0].id])

        ##check to see if the role existed but its not in the tagging list
        if(not rows):
            await message.channel.send("You can't add that role to yourself, nice try!", delete_after=5)
            return


        rows = runsql.executeSQL("SELECT role_id FROM possible_user_roles WHERE server_id = %s and ignore_tag = 0", [message.guild.id])
        roles = discordBotFunctions.getRolesFromIds(message,rows)
        print("ROLES:")
        print(foundRoles)
        await message.author.remove_roles(*roles)
        await message.author.add_roles(foundRoles[0])
        await message.channel.send("You have been successfully been given the role " + foundRoles[0].name + "!", delete_after=5)
        await message.delete()
        

    ##checks to see if the message posted is in the user tagging channel, and if it is then it is deleted
    async def checkIfTagChannel(self, message):
        row = runsql.executeSQL("SELECT 1 FROM user_tag_location WHERE server_id = %s", [message.guild.id])
        if(not row):
            return False

        ##makes sure it isn't a command
        commands = [self.startingChar + "setupusertagginghere",self.startingChar + "stopusertagging",self.startingChar + "addtag ",self.startingChar + "removetag ",self.startingChar + "im ",self.startingChar + "settag ", self.startingChar + "ignoretag ", self.startingChar + "updateusertags ", self.startingChar + "delete "]
        if(any(message.content.startswith(command) for command in commands)):
            return False

        ##delets unwanted messages
        if(message.channel.id in self.taggingChannels):
            await message.channel.send("Excuse me, posts are not allowed in this channel", delete_after = 5)
            await message.delete()
            return True

        foundRole = False
        thisServerRoles = []

        ##checks to see if the user has the roles required for the server
        for role in message.author.roles:
            for row in self.rolesRequired:
                if row['server_id'] == message.guild.id and role.id == row['role_id']:
                    foundRole = True

        ##if its true then the user must be notified
        if(not foundRole):
            for tagMessages in reversed(list(self.tagYourselfMessages)):
                if tagMessages['user_id'] == message.author.id:
                    if datetime.datetime.now() - tagMessages['time'] < datetime.timedelta(minutes=6):
                        return False

            endOfString = []
            rows = runsql.executeSQL("SELECT * FROM possible_user_roles WHERE server_id = %s AND ignore_tag = 0 ORDER BY id DESC", [message.guild.id])
            for role in message.guild.roles:
                for row in rows:
                    if role.id == row['role_id']:
                        endOfString.append("?im " + role.name + "\n")

            if(not endOfString):
                return False
            ##contructs the message to alert the user they don't have the right tags
            row = runsql.executeSQL("SELECT channel_id FROM user_tag_location WHERE server_id = %s", [message.guild.id])
            channel = message.guild.get_channel(row[0]['channel_id'])
            messageString = message.author.mention + " this server requires you to assign a role to yourself. To add a role to yourself type:\n\n" + "".join(endOfString) + "\n"
            messageString += "For more information please visit " + channel.mention
            sentMessage = await message.channel.send(messageString)
            self.tagYourselfMessages.append({"message": sentMessage, "user_id": message.author.id, "time": datetime.datetime.now()})
            return False

        return False