import runsql
import datetime
import printStats
import parsemessage
import time

async def userStats(message):

    users = parsemessage.getUsers(message, getUser = True, allowFullMessage = True)
    if(not users):
        users = [message.author]
    for user in users:
        messageEdit = await message.channel.send("Calculating ...")
        listString = {}
        listString['Title'] = user.display_name
        emoteList = runsql.executeSQL(
            """SELECT emoteName, SUM(count) as Total 
            FROM emote 
            WHERE user_id = %s AND server_id = %s
            GROUP BY emoteName
            ORDER BY Total DESC 
            LIMIT 5""",[user.id, message.guild.id])
        if(not emoteList):
            await message.channel.send("This user has not sent any messages")
            return
        emoteNames = []
        for emote in emoteList:
            emoteNames.append(emote['emoteName'])
        
        listString["Most Popular Emotes"] = ", ".join(emoteNames)
        listString["Joined"] = user.joined_at
        listString["Total Roles"] = len(user.roles) - 1

        earliestMessage = runsql.executeSingleSQL(
            """SELECT min(datetime) as edate 
            FROM message 
            WHERE user_id = %s AND server_id = %s""",[user.id, message.guild.id])

        earliestDate = earliestMessage['edate']
        timeNow = datetime.datetime.now()
        if(earliestDate < timeNow + datetime.timedelta(weeks=-3)):
            earliestDate = timeNow + datetime.timedelta(weeks=-3)

        messageTotal = runsql.executeSingleSQL("""SELECT COUNT(message_id) as Total
            FROM message 
            WHERE user_id = %s AND server_id = %s AND datetime >= %s """,[user.id, message.guild.id, earliestDate])
        
        d2 = datetime.datetime.now()
        diff = d2 - earliestDate
        # Convert to Unix timestamp
        d1_ts = time.mktime(earliestDate.timetuple())
        d2_ts = time.mktime(d2.timetuple())

        minutes = int(d2_ts-d1_ts) / 60
        hours = minutes / 60
        days = hours / 24
        
        listString["Avg messages per day"] = "{:3.2f}".format(messageTotal['Total']/days)
        listString["Avg messages per hour"] = "{:3.2f}".format(messageTotal['Total']/hours)
        listString["Avg messages per minute"] = "{:3.2f}".format(messageTotal['Total']/minutes) 
        

        emotes = runsql.executeSingleSQL("""SELECT SUM(count) as Total
            FROM emote 
            WHERE user_id = %s AND server_id = %s AND datetime >= %s """,[user.id, message.guild.id, earliestDate])
        if not emotes["Total"]:
            emotes["Total"] = 0
        listString["Avg emotes per day"] = "{:3.2f}".format(float(emotes['Total'])/days)
        listString["Avg emotes per hour"] = "{:3.2f}".format(float(emotes['Total'])/hours)
        listString["Avg emotes per minute"] = "{:3.2f}".format(float(emotes['Total'])/minutes) 
        

        messageWithEmotes = runsql.executeSingleSQL("""SELECT Count(message.id) as Total
            FROM message
            LEFT JOIN emote on message.message_id = emote.message_id
            WHERE message.user_id = %s AND emote.user_id = %s
            AND message.server_id = %s AND emote.server_id = %s
            AND message.datetime >= %s AND emote.datetime >= %s""", [user.id, user.id, message.guild.id, message.guild.id, earliestDate, earliestDate])
        
        if messageTotal['Total'] != 0:
            listString["Messages including an emote"] = "{:3.1f}%".format((float(messageWithEmotes['Total'])/messageTotal['Total']) * 100)
        
        singleEmoteMessages = runsql.executeSingleSQL("""SELECT Count(message.id) as Total
            FROM message
            WHERE (message.message REGEXP '^<:[a-zA-Z]+:[0-9]+>$' OR (CHAR_LENGTH(message) = 1 AND message REGEXP '[^\A-Za-z0-9]'))
            AND message.user_id = %s AND message.server_id = %s
            AND datetime >= %s""", [user.id, message.guild.id, earliestDate])

        listString["Single emote messages"] = "{:3.1f}%".format((singleEmoteMessages['Total']/messageTotal['Total']) * 100)

        await printStats.printList(messageEdit,[listString], edit = True)