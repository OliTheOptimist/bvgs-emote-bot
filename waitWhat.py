import os.path
async def runHelpCommands(message):

    ##if there is no message content then ?waitwhat is the only command
    if(not message.content):
        with open ("HelpFiles/commands/waitwhat.txt", "r") as myfile:
            await message.author.send("```" + myfile.read() + "```")
            await message.channel.send("You have been sent a private message with all the commands!!")

        ##if the user has certain privilages give them the extra commands they can use
        with open ("HelpFiles/commands/rolecommands.txt", "r") as myfile:
            await message.author.send("```" + myfile.read() + "```")
        return

    ##if there is more to the command than ?waitwhat find the extra information
    ##does a search in the folder for the command txt file and prints it
    lookUp = message.content.lower()
    if(os.path.isfile("HelpFiles/" + lookUp + ".txt")):
        with open ("HelpFiles/" + lookUp + ".txt", "r") as myfile:
            await message.channel.send("```markdown\n" + myfile.read() + "```")
            return

    ##command not found
    await message.channel.send("That command does not exist, please use ?waitwhat (command) for more information about a certain command")

    
    