import parsemessage
import runsql
import datetime
import printStats
import math

async def weeklyUsage(message,type):
    calculatingMessage = await message.channel.send("Calculating...")

    whereReturn = await parsemessage.parseMessageForSQL(message)

    daysOfTheWeek = ["Sunday",  "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    timeNames = [" 0am"," 2am"," 4am", " 6am", " 8am", "10am", "12pm"," 2pm", " 4pm", " 6pm", " 8pm", "10pm", " 0am"]

    sqlStart = None
    if(type == "emote"):
        sqlStart = "SELECT SUM(count) as total FROM emote WHERE server_id = %s " + whereReturn["String"] + " AND "
    else:
        sqlStart = "SELECT COUNT(id) as total FROM message WHERE server_id = %s " + whereReturn["String"] + " AND "

    tableInfo = []
    ##using sql
    if(True):
        sqlString = ""
        params = []
        for x in range(0, 12):
            sqlString += "SELECT %s as Time"
            params.append(timeNames[x] + " - " + timeNames[x+1])

            for y in range(2, 9):
                if y == 8:
                    y = 1
                sqldates = []
                params = params + [message.guild.id] + whereReturn['Parameters']

                sqlString += ", (" + sqlStart + " DAYOFWEEK(datetime) = " + str(y)
                sqlString += " AND Hour(datetime) BETWEEN " + str(x * 2) + " AND " + str((x + 1) * 2) + ") as %s"
                params.append(daysOfTheWeek[y - 1])
            if(not x == 11):
                sqlString += "\n UNION \n"

        tableInfo = runsql.executeSQL(sqlString, params)
    else:
        ##using python
        for x in range(0, 12):
            await calculatingMessage.edit(content="Calculating... " + str(x) + "/" + str(11))
            rowInfo = {"Time": timeNames[x] + " - " + timeNames[x+1]}
            for y in range(2, 9):
                if y == 8:
                    y = 1
                sqldates = []
                params = [message.guild.id] + whereReturn['Parameters']
                sqlString = sqlStart + " DAYOFWEEK(datetime) = " + str(y)
                sqlString += " AND Hour(datetime) BETWEEN " + str(x * 2) + " AND " + str((x + 1) * 2)
                result = runsql.executeSingleSQL(sqlString, params)

                rowInfo[daysOfTheWeek[y]] = result['total']
            tableInfo.append(rowInfo)

    maxX = math.inf
    maxDay = math.inf
    value = 0
    total = 0

    for x in range(0, len(tableInfo)):
        for day in daysOfTheWeek:
            if not tableInfo[x][day] == None:
                total += tableInfo[x][day]
            else:
                tableInfo[x][day] = 0
            if tableInfo[x][day] > value:
                value = tableInfo[x][day]
                maxX = x
                maxDay = day

    for x in range(0, len(tableInfo)):
        for day in daysOfTheWeek:
            if parsemessage.containsPercentage(message):
                tableInfo[x][day] = str("%.3f" % (tableInfo[x][day] / total))
            if x == maxX and day == maxDay:
                tableInfo[x][day] = "*" + str(tableInfo[x][day]) + "*"

    await printStats.printTable(calculatingMessage, tableInfo, edit=True)

async def getWeeklyUsage(message):
    checkForEmote = await parsemessage.parseMessageForSQL(message)

    if("emotes" in message.content or checkForEmote["valuesFound"]["emote"]):
        await weeklyUsage(message, "emote")
    else:
        await weeklyUsage(message, "message")




        

