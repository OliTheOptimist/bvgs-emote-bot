from os import path
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import runsql
import parsemessage
import random
import os
import discord

from wordcloud import WordCloud, STOPWORDS
currentColour = -1
##creates a set of colours that the wordcloud randomly picks from
def colour_function(word, font_size, position, orientation, random_state=None,
					**kwargs):
	global currentColour
	currentColour += 1
	rainbowColours = ["#E6C229","#F17105","#D11149","#6610F2","#1A8FE3","#7CB518","#AF3800","#00CFC1"]
	return rainbowColours[currentColour%len(rainbowColours)]

async def createWordCloud(message):
	async with message.channel.typing():
		text = ""
		##gets sql information from the message
		messageSQL = await parsemessage.parseMessageForSQL(message, emotes = False)
		users = parsemessage.getUsers(message)
		usersTagged = []
		rolesTagged = []
		channelsTagged = []
		for user in users:
			usersTagged.append(user.display_name)

		roles = parsemessage.getRoles(message)
		for role in roles:
			rolesTagged.append(role.name)

		channels = message.channel_mentions
		for channel in channels:
			channelsTagged.append(channel.mention)

		wordCloudMessage = ""
		if not usersTagged and not rolesTagged and not channelsTagged:
			wordCloudMessage = "Wordcloud for the whole of " + message.guild.name
		else:
			if usersTagged:
				wordCloudMessage += "This wordcloud is for the following user(s): " + ",".join(usersTagged) + "\n"
			if rolesTagged:
				wordCloudMessage += "This wordcloud is for the following role(s): " + ",".join(rolesTagged) + "\n"
			if channelsTagged:
				wordCloudMessage += "This wordcloud is for the following channel(s): " + ",".join(channelsTagged) + "\n"


		##selects the information required
		rows = runsql.executeSQL("SELECT message FROM message WHERE server_id = %s " + messageSQL['String'] + " ORDER BY id DESC LIMIT 10000 ", [message.guild.id] + messageSQL['Parameters'])
		##adds it to text with a space to distinguish between messages
		for row in rows:
			if "https" in row['message']:
				continue
			text += "\n" + parsemessage.replaceIDsWithTags(message, row['message'])

		##creates a wordcloud with some settings
		wc = WordCloud(font_path="./WorkSans-Regular.ttf",max_words=500,background_color="black",width= 1920, height=1080, margin=15, min_font_size=14,color_func=colour_function, scale=1, collocations=False)
		# generate word cloud
		wc.generate(text)

		# store to file
		fileLocation = "wordclouds/" + str(message.author.id) + ".png"
		wc.to_file(fileLocation)
		await message.channel.send(content=wordCloudMessage, file=discord.File(fileLocation))
		os.remove(fileLocation)
		##should probably delete the file but yolo